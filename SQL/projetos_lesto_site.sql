-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 13-Ago-2019 às 17:38
-- Versão do servidor: 5.7.23
-- versão do PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projetos_lesto_site`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `le_commentmeta`
--

DROP TABLE IF EXISTS `le_commentmeta`;
CREATE TABLE IF NOT EXISTS `le_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `le_comments`
--

DROP TABLE IF EXISTS `le_comments`;
CREATE TABLE IF NOT EXISTS `le_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10))
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `le_comments`
--

INSERT INTO `le_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Um comentarista do WordPress', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2019-08-08 16:19:51', '2019-08-08 19:19:51', 'Olá, isso é um comentário.\nPara começar a moderar, editar e excluir comentários, visite a tela de Comentários no painel.\nAvatares de comentaristas vêm a partir do <a href=\"https://gravatar.com\">Gravatar</a>.', 0, 'post-trashed', '', '', 0, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `le_links`
--

DROP TABLE IF EXISTS `le_links`;
CREATE TABLE IF NOT EXISTS `le_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `le_options`
--

DROP TABLE IF EXISTS `le_options`;
CREATE TABLE IF NOT EXISTS `le_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=MyISAM AUTO_INCREMENT=298 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `le_options`
--

INSERT INTO `le_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/projetos/projeto_lesto_site', 'yes'),
(2, 'home', 'http://localhost/projetos/projeto_lesto_site', 'yes'),
(3, 'blogname', 'Lesto escritórios', 'yes'),
(4, 'blogdescription', 'Lesto escritórios', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'devhcdesenvolvimentos@gmail.com', 'yes'),
(7, 'start_of_week', '0', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '9', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '9', 'yes'),
(23, 'date_format', 'j \\d\\e F \\d\\e Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'j \\d\\e F \\d\\e Y, H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:189:{s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:11:\"destaque/?$\";s:28:\"index.php?post_type=destaque\";s:41:\"destaque/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=destaque&feed=$matches[1]\";s:36:\"destaque/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=destaque&feed=$matches[1]\";s:28:\"destaque/page/([0-9]{1,})/?$\";s:46:\"index.php?post_type=destaque&paged=$matches[1]\";s:8:\"plano/?$\";s:25:\"index.php?post_type=plano\";s:38:\"plano/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?post_type=plano&feed=$matches[1]\";s:33:\"plano/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?post_type=plano&feed=$matches[1]\";s:25:\"plano/page/([0-9]{1,})/?$\";s:43:\"index.php?post_type=plano&paged=$matches[1]\";s:7:\"sala/?$\";s:24:\"index.php?post_type=sala\";s:37:\"sala/feed/(feed|rdf|rss|rss2|atom)/?$\";s:41:\"index.php?post_type=sala&feed=$matches[1]\";s:32:\"sala/(feed|rdf|rss|rss2|atom)/?$\";s:41:\"index.php?post_type=sala&feed=$matches[1]\";s:24:\"sala/page/([0-9]{1,})/?$\";s:42:\"index.php?post_type=sala&paged=$matches[1]\";s:10:\"unidade/?$\";s:27:\"index.php?post_type=unidade\";s:40:\"unidade/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=unidade&feed=$matches[1]\";s:35:\"unidade/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=unidade&feed=$matches[1]\";s:27:\"unidade/page/([0-9]{1,})/?$\";s:45:\"index.php?post_type=unidade&paged=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:36:\"destaque/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:46:\"destaque/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:66:\"destaque/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"destaque/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"destaque/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:42:\"destaque/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:25:\"destaque/([^/]+)/embed/?$\";s:41:\"index.php?destaque=$matches[1]&embed=true\";s:29:\"destaque/([^/]+)/trackback/?$\";s:35:\"index.php?destaque=$matches[1]&tb=1\";s:49:\"destaque/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?destaque=$matches[1]&feed=$matches[2]\";s:44:\"destaque/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?destaque=$matches[1]&feed=$matches[2]\";s:37:\"destaque/([^/]+)/page/?([0-9]{1,})/?$\";s:48:\"index.php?destaque=$matches[1]&paged=$matches[2]\";s:44:\"destaque/([^/]+)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?destaque=$matches[1]&cpage=$matches[2]\";s:33:\"destaque/([^/]+)(?:/([0-9]+))?/?$\";s:47:\"index.php?destaque=$matches[1]&page=$matches[2]\";s:25:\"destaque/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:35:\"destaque/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:55:\"destaque/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"destaque/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"destaque/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:31:\"destaque/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:33:\"plano/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:43:\"plano/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:63:\"plano/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:58:\"plano/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:58:\"plano/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:39:\"plano/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:22:\"plano/([^/]+)/embed/?$\";s:38:\"index.php?plano=$matches[1]&embed=true\";s:26:\"plano/([^/]+)/trackback/?$\";s:32:\"index.php?plano=$matches[1]&tb=1\";s:46:\"plano/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?plano=$matches[1]&feed=$matches[2]\";s:41:\"plano/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?plano=$matches[1]&feed=$matches[2]\";s:34:\"plano/([^/]+)/page/?([0-9]{1,})/?$\";s:45:\"index.php?plano=$matches[1]&paged=$matches[2]\";s:41:\"plano/([^/]+)/comment-page-([0-9]{1,})/?$\";s:45:\"index.php?plano=$matches[1]&cpage=$matches[2]\";s:30:\"plano/([^/]+)(?:/([0-9]+))?/?$\";s:44:\"index.php?plano=$matches[1]&page=$matches[2]\";s:22:\"plano/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:32:\"plano/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:52:\"plano/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:47:\"plano/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:47:\"plano/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:28:\"plano/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:32:\"sala/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:42:\"sala/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:62:\"sala/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:57:\"sala/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:57:\"sala/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:38:\"sala/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:21:\"sala/([^/]+)/embed/?$\";s:37:\"index.php?sala=$matches[1]&embed=true\";s:25:\"sala/([^/]+)/trackback/?$\";s:31:\"index.php?sala=$matches[1]&tb=1\";s:45:\"sala/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?sala=$matches[1]&feed=$matches[2]\";s:40:\"sala/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?sala=$matches[1]&feed=$matches[2]\";s:33:\"sala/([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?sala=$matches[1]&paged=$matches[2]\";s:40:\"sala/([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?sala=$matches[1]&cpage=$matches[2]\";s:29:\"sala/([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?sala=$matches[1]&page=$matches[2]\";s:21:\"sala/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:31:\"sala/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:51:\"sala/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:46:\"sala/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:46:\"sala/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:27:\"sala/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:35:\"unidade/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:45:\"unidade/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:65:\"unidade/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"unidade/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"unidade/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:41:\"unidade/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:24:\"unidade/([^/]+)/embed/?$\";s:40:\"index.php?unidade=$matches[1]&embed=true\";s:28:\"unidade/([^/]+)/trackback/?$\";s:34:\"index.php?unidade=$matches[1]&tb=1\";s:48:\"unidade/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?unidade=$matches[1]&feed=$matches[2]\";s:43:\"unidade/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?unidade=$matches[1]&feed=$matches[2]\";s:36:\"unidade/([^/]+)/page/?([0-9]{1,})/?$\";s:47:\"index.php?unidade=$matches[1]&paged=$matches[2]\";s:43:\"unidade/([^/]+)/comment-page-([0-9]{1,})/?$\";s:47:\"index.php?unidade=$matches[1]&cpage=$matches[2]\";s:32:\"unidade/([^/]+)(?:/([0-9]+))?/?$\";s:46:\"index.php?unidade=$matches[1]&page=$matches[2]\";s:24:\"unidade/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:34:\"unidade/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:54:\"unidade/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"unidade/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"unidade/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:30:\"unidade/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:59:\"categoria-destaque/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:56:\"index.php?categoriaDestaque=$matches[1]&feed=$matches[2]\";s:54:\"categoria-destaque/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:56:\"index.php?categoriaDestaque=$matches[1]&feed=$matches[2]\";s:35:\"categoria-destaque/([^/]+)/embed/?$\";s:50:\"index.php?categoriaDestaque=$matches[1]&embed=true\";s:47:\"categoria-destaque/([^/]+)/page/?([0-9]{1,})/?$\";s:57:\"index.php?categoriaDestaque=$matches[1]&paged=$matches[2]\";s:29:\"categoria-destaque/([^/]+)/?$\";s:39:\"index.php?categoriaDestaque=$matches[1]\";s:56:\"categoria-salas/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:53:\"index.php?categoriaSalas=$matches[1]&feed=$matches[2]\";s:51:\"categoria-salas/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:53:\"index.php?categoriaSalas=$matches[1]&feed=$matches[2]\";s:32:\"categoria-salas/([^/]+)/embed/?$\";s:47:\"index.php?categoriaSalas=$matches[1]&embed=true\";s:44:\"categoria-salas/([^/]+)/page/?([0-9]{1,})/?$\";s:54:\"index.php?categoriaSalas=$matches[1]&paged=$matches[2]\";s:26:\"categoria-salas/([^/]+)/?$\";s:36:\"index.php?categoriaSalas=$matches[1]\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:39:\"index.php?&page_id=18&cpage=$matches[1]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";s:27:\"[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\"[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\"[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\"[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\"[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"([^/]+)/embed/?$\";s:37:\"index.php?name=$matches[1]&embed=true\";s:20:\"([^/]+)/trackback/?$\";s:31:\"index.php?name=$matches[1]&tb=1\";s:40:\"([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:35:\"([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?name=$matches[1]&feed=$matches[2]\";s:28:\"([^/]+)/page/?([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&paged=$matches[2]\";s:35:\"([^/]+)/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?name=$matches[1]&cpage=$matches[2]\";s:24:\"([^/]+)(?:/([0-9]+))?/?$\";s:43:\"index.php?name=$matches[1]&page=$matches[2]\";s:16:\"[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:26:\"[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:46:\"[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:41:\"[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:22:\"[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:7:{i:0;s:35:\"redux-framework/redux-framework.php\";i:1;s:27:\"base-modelo/base-modelo.php\";i:2;s:36:\"contact-form-7/wp-contact-form-7.php\";i:3;s:32:\"disqus-comment-system/disqus.php\";i:4;s:33:\"duplicate-post/duplicate-post.php\";i:5;s:21:\"meta-box/meta-box.php\";i:6;s:37:\"post-types-order/post-types-order.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'lestoescritorios', 'yes'),
(41, 'stylesheet', 'lestoescritorios', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '44719', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'none', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:2:{i:2;a:4:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:12:\"hierarchical\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:0:{}', 'yes'),
(80, 'widget_rss', 'a:0:{}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', 'America/Sao_Paulo', 'yes'),
(83, 'page_for_posts', '47', 'yes'),
(84, 'page_on_front', '18', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '1', 'yes'),
(93, 'initial_db_version', '44719', 'yes'),
(94, 'le_user_roles', 'a:5:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:62:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:10:\"copy_posts\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:35:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:10:\"copy_posts\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'WPLANG', 'pt_BR', 'yes'),
(97, 'widget_search', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-posts', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_recent-comments', 'a:2:{i:2;a:2:{s:5:\"title\";s:0:\"\";s:6:\"number\";i:5;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_archives', 'a:2:{i:2;a:3:{s:5:\"title\";s:0:\"\";s:5:\"count\";i:0;s:8:\"dropdown\";i:0;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_meta', 'a:2:{i:2;a:1:{s:5:\"title\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'sidebars_widgets', 'a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(103, 'cron', 'a:5:{i:1565720391;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1565723991;a:4:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1565724049;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1565724051;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}s:7:\"version\";i:2;}', 'yes'),
(104, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(132, 'can_compress_scripts', '1', 'no'),
(114, 'recovery_keys', 'a:0:{}', 'yes'),
(116, 'theme_mods_twentynineteen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1565294234;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(286, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.2.2.zip\";s:6:\"locale\";s:5:\"pt_BR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.2.2.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.2.2\";s:7:\"version\";s:5:\"5.2.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1565715539;s:15:\"version_checked\";s:5:\"5.2.2\";s:12:\"translations\";a:0:{}}', 'no'),
(125, '_site_transient_timeout_browser_5eaddbe64bb311a7ba788adfd9ffdfcb', '1565896850', 'no'),
(126, '_site_transient_browser_5eaddbe64bb311a7ba788adfd9ffdfcb', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"75.0.3770.142\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(127, '_site_transient_timeout_php_check_464f4068caea2f8f3edcc5ae59429c65', '1565896851', 'no'),
(128, '_site_transient_php_check_464f4068caea2f8f3edcc5ae59429c65', 'a:5:{s:19:\"recommended_version\";s:3:\"7.3\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(284, '_site_transient_timeout_theme_roots', '1565717337', 'no'),
(285, '_site_transient_theme_roots', 'a:2:{s:16:\"lestoescritorios\";s:7:\"/themes\";s:14:\"twentynineteen\";s:7:\"/themes\";}', 'no'),
(287, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1565715540;s:7:\"checked\";a:2:{s:16:\"lestoescritorios\";s:5:\"1.0.0\";s:14:\"twentynineteen\";s:3:\"1.4\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:1:{i:0;a:7:{s:4:\"type\";s:5:\"theme\";s:4:\"slug\";s:14:\"twentynineteen\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:3:\"1.4\";s:7:\"updated\";s:19:\"2019-06-13 00:57:02\";s:7:\"package\";s:78:\"https://downloads.wordpress.org/translation/theme/twentynineteen/1.4/pt_BR.zip\";s:10:\"autoupdate\";b:1;}}}', 'no'),
(145, 'current_theme', 'lestoescritorios', 'yes'),
(146, 'theme_mods_lestoescritorios', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:6:\"menu-1\";i:2;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(147, 'theme_switched', '', 'yes'),
(245, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(261, '_site_transient_timeout_browser_666332998c62658cc43216116351bf1f', '1566237015', 'no'),
(262, '_site_transient_browser_666332998c62658cc43216116351bf1f', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"76.0.3809.100\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(150, 'recently_activated', 'a:0:{}', 'yes'),
(162, 'wpcf7', 'a:2:{s:7:\"version\";s:5:\"5.1.4\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";d:1565283858;s:7:\"version\";s:5:\"5.1.4\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes'),
(163, 'disqus_sync_token', '86125c7cfb067a60b08369a0acc8a507', 'yes'),
(164, 'duplicate_post_copytitle', '1', 'yes'),
(165, 'duplicate_post_copydate', '0', 'yes'),
(166, 'duplicate_post_copystatus', '0', 'yes'),
(167, 'duplicate_post_copyslug', '0', 'yes'),
(168, 'duplicate_post_copyexcerpt', '1', 'yes'),
(169, 'duplicate_post_copycontent', '1', 'yes'),
(170, 'duplicate_post_copythumbnail', '1', 'yes'),
(171, 'duplicate_post_copytemplate', '1', 'yes'),
(172, 'duplicate_post_copyformat', '1', 'yes'),
(173, 'duplicate_post_copyauthor', '0', 'yes'),
(174, 'duplicate_post_copypassword', '0', 'yes'),
(175, 'duplicate_post_copyattachments', '0', 'yes'),
(176, 'duplicate_post_copychildren', '0', 'yes'),
(177, 'duplicate_post_copycomments', '0', 'yes'),
(178, 'duplicate_post_copymenuorder', '1', 'yes'),
(179, 'duplicate_post_taxonomies_blacklist', 'a:0:{}', 'yes'),
(180, 'duplicate_post_blacklist', '', 'yes'),
(181, 'duplicate_post_types_enabled', 'a:2:{i:0;s:4:\"post\";i:1;s:4:\"page\";}', 'yes'),
(182, 'duplicate_post_show_row', '1', 'yes'),
(183, 'duplicate_post_show_adminbar', '1', 'yes'),
(184, 'duplicate_post_show_submitbox', '1', 'yes'),
(185, 'duplicate_post_show_bulkactions', '1', 'yes'),
(186, 'duplicate_post_version', '3.2.3', 'yes'),
(187, 'duplicate_post_show_notice', '0', 'no'),
(190, 'cpto_options', 'a:7:{s:23:\"show_reorder_interfaces\";a:3:{s:4:\"post\";s:4:\"show\";s:10:\"attachment\";s:4:\"show\";s:8:\"wp_block\";s:4:\"show\";}s:8:\"autosort\";i:1;s:9:\"adminsort\";i:1;s:18:\"use_query_ASC_DESC\";s:0:\"\";s:17:\"archive_drag_drop\";i:1;s:10:\"capability\";s:14:\"manage_options\";s:21:\"navigation_sort_apply\";i:1;}', 'yes'),
(191, 'CPT_configured', 'TRUE', 'yes'),
(195, 'category_children', 'a:0:{}', 'yes'),
(206, 'new_admin_email', 'devhcdesenvolvimentos@gmail.com', 'yes'),
(277, 'categoriaSalas_children', 'a:0:{}', 'yes'),
(218, 'redux_version_upgraded_from', '3.6.15', 'yes'),
(219, 'configuracao', 'a:34:{s:8:\"last_tab\";s:1:\"1\";s:8:\"opt_logo\";a:9:{s:3:\"url\";s:86:\"http://localhost/projetos/projeto_lesto_site/wp-content/uploads/2019/08/logo-lesto.png\";s:2:\"id\";s:2:\"35\";s:6:\"height\";s:2:\"63\";s:5:\"width\";s:3:\"205\";s:9:\"thumbnail\";s:93:\"http://localhost/projetos/projeto_lesto_site/wp-content/uploads/2019/08/logo-lesto-150x63.png\";s:5:\"title\";s:10:\"logo-lesto\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:12:\"opt_endereco\";s:0:\"\";s:12:\"opt_telefone\";s:0:\"\";s:12:\"opt_facebook\";s:0:\"\";s:10:\"opt_google\";s:0:\"\";s:13:\"opt_instagram\";s:0:\"\";s:12:\"opt_linkedin\";s:0:\"\";s:11:\"opt_twitter\";s:0:\"\";s:12:\"opt_whatsapp\";s:0:\"\";s:16:\"opt_informacoes1\";s:61:\"Av. Cândido de Abreu, 470 14º andar – Sala 1407, Curitiba\";s:16:\"opt_informacoes2\";s:47:\"R. Trajano Reis, 472 - São Francisco, Curitiba\";s:9:\"opt_email\";s:20:\"contato@lesto.com.br\";s:19:\"opt_telefone_footer\";s:0:\"\";s:13:\"opt_copyright\";s:47:\"Todos os direitos reservados - CNPJ 00100123456\";s:18:\"opt_titulo_contato\";s:0:\"\";s:21:\"opt_subtitulo_contato\";s:0:\"\";s:16:\"opt_desc_contato\";s:0:\"\";s:18:\"opt_titulo_inicial\";s:73:\"Estabeleça seu escritório em um lugar privilegiado pagando muito pouco.\";s:17:\"opt_texto_inicial\";s:548:\"A lesto Escritotio oferece opções de planos virtuais que atendem as necessidades de profissionais leberais ou autônomos até grande empresas.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in volup.\r\n\r\n<strong>Conheça abaixo nossos planos de escritório virtual e escolha o que melhor atende as suas necessidades</strong>\";s:19:\"opt_galeria_inicial\";s:0:\"\";s:25:\"opt_titulo_planos_inicial\";s:6:\"Planos\";s:24:\"opt_titulo_salas_inicial\";s:5:\"Salas\";s:27:\"opt_titulo_unidades_inicial\";s:8:\"Unidades\";s:17:\"opt_titulo_planos\";s:6:\"Planos\";s:17:\"opt_titulo1_salas\";s:0:\"\";s:15:\"opt_desc1_salas\";s:0:\"\";s:17:\"opt_titulo2_salas\";s:0:\"\";s:15:\"opt_desc2_salas\";s:0:\"\";s:27:\"opt_atendimento_localizacao\";s:0:\"\";s:16:\"opt_titulo_dicas\";s:0:\"\";s:14:\"opt_desc_dicas\";s:0:\"\";s:23:\"opt_titulo_relacionados\";s:0:\"\";s:19:\"opt-customizer-only\";s:1:\"2\";}', 'yes'),
(220, 'configuracao-transients', 'a:2:{s:14:\"changed_values\";a:1:{s:13:\"opt_copyright\";s:0:\"\";}s:9:\"last_save\";i:1565383274;}', 'yes'),
(288, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1565715541;s:7:\"checked\";a:10:{s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";s:7:\"4.3.9.4\";s:27:\"base-modelo/base-modelo.php\";s:3:\"0.1\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:5:\"5.1.4\";s:32:\"disqus-comment-system/disqus.php\";s:6:\"3.0.17\";s:33:\"duplicate-post/duplicate-post.php\";s:5:\"3.2.3\";s:43:\"google-analytics-dashboard-for-wp/gadwp.php\";s:5:\"5.3.8\";s:21:\"meta-box/meta-box.php\";s:5:\"5.0.1\";s:37:\"post-types-order/post-types-order.php\";s:7:\"1.9.4.1\";s:35:\"redux-framework/redux-framework.php\";s:6:\"3.6.15\";s:24:\"wordpress-seo/wp-seo.php\";s:4:\"11.8\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:5:{i:0;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:14:\"contact-form-7\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:5:\"5.1.4\";s:7:\"updated\";s:19:\"2019-08-09 13:03:34\";s:7:\"package\";s:81:\"https://downloads.wordpress.org/translation/plugin/contact-form-7/5.1.4/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:1;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:14:\"duplicate-post\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:5:\"3.2.3\";s:7:\"updated\";s:19:\"2019-07-10 14:30:23\";s:7:\"package\";s:81:\"https://downloads.wordpress.org/translation/plugin/duplicate-post/3.2.3/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:2;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:33:\"google-analytics-dashboard-for-wp\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:5:\"5.3.8\";s:7:\"updated\";s:19:\"2018-06-12 15:42:58\";s:7:\"package\";s:100:\"https://downloads.wordpress.org/translation/plugin/google-analytics-dashboard-for-wp/5.3.8/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:3;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:8:\"meta-box\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:6:\"4.18.4\";s:7:\"updated\";s:19:\"2019-07-13 02:22:41\";s:7:\"package\";s:76:\"https://downloads.wordpress.org/translation/plugin/meta-box/4.18.4/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:4;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:13:\"wordpress-seo\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:4:\"11.8\";s:7:\"updated\";s:19:\"2019-08-12 18:09:39\";s:7:\"package\";s:79:\"https://downloads.wordpress.org/translation/plugin/wordpress-seo/11.8/pt_BR.zip\";s:10:\"autoupdate\";b:1;}}s:9:\"no_update\";a:9:{s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:49:\"w.org/plugins/all-in-one-wp-security-and-firewall\";s:4:\"slug\";s:35:\"all-in-one-wp-security-and-firewall\";s:6:\"plugin\";s:51:\"all-in-one-wp-security-and-firewall/wp-security.php\";s:11:\"new_version\";s:7:\"4.3.9.4\";s:3:\"url\";s:66:\"https://wordpress.org/plugins/all-in-one-wp-security-and-firewall/\";s:7:\"package\";s:78:\"https://downloads.wordpress.org/plugin/all-in-one-wp-security-and-firewall.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:88:\"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/icon-128x128.png?rev=1232826\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:91:\"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/banner-1544x500.png?rev=1914011\";s:2:\"1x\";s:90:\"https://ps.w.org/all-in-one-wp-security-and-firewall/assets/banner-772x250.png?rev=1914013\";}s:11:\"banners_rtl\";a:0:{}}s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:5:\"5.1.4\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/contact-form-7.5.1.4.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=984007\";s:2:\"1x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-128x128.png?rev=984007\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";}s:11:\"banners_rtl\";a:0:{}}s:32:\"disqus-comment-system/disqus.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:35:\"w.org/plugins/disqus-comment-system\";s:4:\"slug\";s:21:\"disqus-comment-system\";s:6:\"plugin\";s:32:\"disqus-comment-system/disqus.php\";s:11:\"new_version\";s:6:\"3.0.17\";s:3:\"url\";s:52:\"https://wordpress.org/plugins/disqus-comment-system/\";s:7:\"package\";s:71:\"https://downloads.wordpress.org/plugin/disqus-comment-system.3.0.17.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:74:\"https://ps.w.org/disqus-comment-system/assets/icon-256x256.png?rev=1012448\";s:2:\"1x\";s:66:\"https://ps.w.org/disqus-comment-system/assets/icon.svg?rev=1636350\";s:3:\"svg\";s:66:\"https://ps.w.org/disqus-comment-system/assets/icon.svg?rev=1636350\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:76:\"https://ps.w.org/disqus-comment-system/assets/banner-772x250.png?rev=1636350\";}s:11:\"banners_rtl\";a:0:{}}s:33:\"duplicate-post/duplicate-post.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/duplicate-post\";s:4:\"slug\";s:14:\"duplicate-post\";s:6:\"plugin\";s:33:\"duplicate-post/duplicate-post.php\";s:11:\"new_version\";s:5:\"3.2.3\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/duplicate-post/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/duplicate-post.3.2.3.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/duplicate-post/assets/icon-256x256.png?rev=1612753\";s:2:\"1x\";s:67:\"https://ps.w.org/duplicate-post/assets/icon-128x128.png?rev=1612753\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/duplicate-post/assets/banner-772x250.png?rev=1612986\";}s:11:\"banners_rtl\";a:0:{}}s:43:\"google-analytics-dashboard-for-wp/gadwp.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:47:\"w.org/plugins/google-analytics-dashboard-for-wp\";s:4:\"slug\";s:33:\"google-analytics-dashboard-for-wp\";s:6:\"plugin\";s:43:\"google-analytics-dashboard-for-wp/gadwp.php\";s:11:\"new_version\";s:5:\"5.3.8\";s:3:\"url\";s:64:\"https://wordpress.org/plugins/google-analytics-dashboard-for-wp/\";s:7:\"package\";s:82:\"https://downloads.wordpress.org/plugin/google-analytics-dashboard-for-wp.5.3.8.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:85:\"https://ps.w.org/google-analytics-dashboard-for-wp/assets/icon-256x256.png?rev=970326\";s:2:\"1x\";s:85:\"https://ps.w.org/google-analytics-dashboard-for-wp/assets/icon-128x128.png?rev=970326\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:88:\"https://ps.w.org/google-analytics-dashboard-for-wp/assets/banner-772x250.png?rev=1064664\";}s:11:\"banners_rtl\";a:0:{}}s:21:\"meta-box/meta-box.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:22:\"w.org/plugins/meta-box\";s:4:\"slug\";s:8:\"meta-box\";s:6:\"plugin\";s:21:\"meta-box/meta-box.php\";s:11:\"new_version\";s:5:\"5.0.1\";s:3:\"url\";s:39:\"https://wordpress.org/plugins/meta-box/\";s:7:\"package\";s:57:\"https://downloads.wordpress.org/plugin/meta-box.5.0.1.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/meta-box/assets/icon-128x128.png?rev=1100915\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:63:\"https://ps.w.org/meta-box/assets/banner-772x250.png?rev=1929588\";}s:11:\"banners_rtl\";a:0:{}}s:37:\"post-types-order/post-types-order.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:30:\"w.org/plugins/post-types-order\";s:4:\"slug\";s:16:\"post-types-order\";s:6:\"plugin\";s:37:\"post-types-order/post-types-order.php\";s:11:\"new_version\";s:7:\"1.9.4.1\";s:3:\"url\";s:47:\"https://wordpress.org/plugins/post-types-order/\";s:7:\"package\";s:67:\"https://downloads.wordpress.org/plugin/post-types-order.1.9.4.1.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/post-types-order/assets/icon-128x128.png?rev=1226428\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:72:\"https://ps.w.org/post-types-order/assets/banner-1544x500.png?rev=1675574\";s:2:\"1x\";s:71:\"https://ps.w.org/post-types-order/assets/banner-772x250.png?rev=1429949\";}s:11:\"banners_rtl\";a:0:{}}s:35:\"redux-framework/redux-framework.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:29:\"w.org/plugins/redux-framework\";s:4:\"slug\";s:15:\"redux-framework\";s:6:\"plugin\";s:35:\"redux-framework/redux-framework.php\";s:11:\"new_version\";s:6:\"3.6.15\";s:3:\"url\";s:46:\"https://wordpress.org/plugins/redux-framework/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/redux-framework.3.6.15.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:67:\"https://ps.w.org/redux-framework/assets/icon-256x256.png?rev=995554\";s:2:\"1x\";s:59:\"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554\";s:3:\"svg\";s:59:\"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/redux-framework/assets/banner-772x250.png?rev=793165\";}s:11:\"banners_rtl\";a:0:{}}s:24:\"wordpress-seo/wp-seo.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:27:\"w.org/plugins/wordpress-seo\";s:4:\"slug\";s:13:\"wordpress-seo\";s:6:\"plugin\";s:24:\"wordpress-seo/wp-seo.php\";s:11:\"new_version\";s:4:\"11.8\";s:3:\"url\";s:44:\"https://wordpress.org/plugins/wordpress-seo/\";s:7:\"package\";s:61:\"https://downloads.wordpress.org/plugin/wordpress-seo.11.8.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:66:\"https://ps.w.org/wordpress-seo/assets/icon-256x256.png?rev=1834347\";s:2:\"1x\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=1946641\";s:3:\"svg\";s:58:\"https://ps.w.org/wordpress-seo/assets/icon.svg?rev=1946641\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500.png?rev=1843435\";s:2:\"1x\";s:68:\"https://ps.w.org/wordpress-seo/assets/banner-772x250.png?rev=1843435\";}s:11:\"banners_rtl\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/wordpress-seo/assets/banner-1544x500-rtl.png?rev=1843435\";s:2:\"1x\";s:72:\"https://ps.w.org/wordpress-seo/assets/banner-772x250-rtl.png?rev=1843435\";}}}}', 'no');

-- --------------------------------------------------------

--
-- Estrutura da tabela `le_postmeta`
--

DROP TABLE IF EXISTS `le_postmeta`;
CREATE TABLE IF NOT EXISTS `le_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM AUTO_INCREMENT=285 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `le_postmeta`
--

INSERT INTO `le_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 5, '_form', '<div class=\"info\">\n<label for=\"nome\">Seu Nome</label>\n[text* nome id:nome class:nome placeholder \"Nome\"]\n</div>\n<div  class=\"info\">\n<label for=\"email\">Seu e-email</label>\n[email* email id:email class:email placeholder \"Seu e-mail\"]\n</div>\n<div  class=\"info full\">\n<label for=\"assunto\">Assunto</label>\n[text* assunto id:assunto class:assunto placeholder \"Assunto\"]\n</div>\n<div  class=\"info full\">\n<label for=\"mensagem\" >Mensagem</label>\n[textarea* mensagem id:mensagem class:mensagem placeholder \"Mesagem\"]\n</div>\n<div class=\"btn-enviar\">\n[submit id:Enviar class:Enviar \"Enviar\"]\n</div>'),
(4, 5, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:35:\"Lesto escritórios \"[your-subject]\"\";s:6:\"sender\";s:52:\"Lesto escritórios <devhcdesenvolvimentos@gmail.com>\";s:9:\"recipient\";s:31:\"devhcdesenvolvimentos@gmail.com\";s:4:\"body\";s:180:\"De: [nome]\nEmail: [email]\nAssunto: [assunto]\n\nMensagem:\n[mensagem]\n-- \nThis e-mail was sent from a contact form on Lesto escritórios (http://localhost/projetos/projeto_lesto_site)\";s:18:\"additional_headers\";s:0:\"\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(5, 5, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:35:\"Lesto escritórios \"[your-subject]\"\";s:6:\"sender\";s:52:\"Lesto escritórios <devhcdesenvolvimentos@gmail.com>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:143:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Lesto escritórios (http://localhost/projetos/projeto_lesto_site)\";s:18:\"additional_headers\";s:41:\"Reply-To: devhcdesenvolvimentos@gmail.com\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(6, 5, '_messages', 'a:23:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";s:12:\"invalid_date\";s:29:\"The date format is incorrect.\";s:14:\"date_too_early\";s:44:\"The date is before the earliest one allowed.\";s:13:\"date_too_late\";s:41:\"The date is after the latest one allowed.\";s:13:\"upload_failed\";s:46:\"There was an unknown error uploading the file.\";s:24:\"upload_file_type_invalid\";s:49:\"You are not allowed to upload files of this type.\";s:21:\"upload_file_too_large\";s:20:\"The file is too big.\";s:23:\"upload_failed_php_error\";s:38:\"There was an error uploading the file.\";s:14:\"invalid_number\";s:29:\"The number format is invalid.\";s:16:\"number_too_small\";s:47:\"The number is smaller than the minimum allowed.\";s:16:\"number_too_large\";s:46:\"The number is larger than the maximum allowed.\";s:23:\"quiz_answer_not_correct\";s:36:\"The answer to the quiz is incorrect.\";s:17:\"captcha_not_match\";s:31:\"Your entered code is incorrect.\";s:13:\"invalid_email\";s:38:\"The e-mail address entered is invalid.\";s:11:\"invalid_url\";s:19:\"The URL is invalid.\";s:11:\"invalid_tel\";s:32:\"The telephone number is invalid.\";}'),
(7, 5, '_additional_settings', ''),
(8, 5, '_locale', 'pt_BR'),
(9, 3, '_wp_trash_meta_status', 'draft'),
(10, 3, '_wp_trash_meta_time', '1565298867'),
(11, 3, '_wp_desired_post_slug', 'politica-de-privacidade'),
(12, 2, '_wp_trash_meta_status', 'publish'),
(13, 2, '_wp_trash_meta_time', '1565298867'),
(14, 2, '_wp_desired_post_slug', 'pagina-exemplo'),
(15, 18, '_edit_lock', '1565298746:1'),
(16, 18, '_wp_page_template', 'paginas/inicial.php'),
(17, 20, '_edit_lock', '1565298761:1'),
(18, 20, '_wp_page_template', 'paginas/contato.php'),
(19, 22, '_edit_lock', '1565643949:1'),
(20, 22, '_wp_page_template', 'paginas/sala.php'),
(21, 24, '_edit_lock', '1565298786:1'),
(22, 24, '_wp_page_template', 'paginas/planos.php'),
(23, 26, '_edit_lock', '1565383068:1'),
(24, 26, '_wp_page_template', 'paginas/localizacao.php'),
(26, 28, '_wp_attached_file', '2019/08/bannerTopoHome.jpg'),
(27, 28, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:1080;s:4:\"file\";s:26:\"2019/08/bannerTopoHome.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:26:\"bannerTopoHome-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:26:\"bannerTopoHome-300x169.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:169;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:26:\"bannerTopoHome-768x432.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:432;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:27:\"bannerTopoHome-1024x576.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:576;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"1\";s:8:\"keywords\";a:0:{}}}'),
(28, 29, '_wp_attached_file', '2019/08/carrossel.png'),
(29, 29, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:527;s:6:\"height\";i:350;s:4:\"file\";s:21:\"2019/08/carrossel.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"carrossel-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"carrossel-300x199.png\";s:5:\"width\";i:300;s:6:\"height\";i:199;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(41, 37, '_menu_item_type', 'post_type'),
(31, 31, '_wp_attached_file', '2019/08/escritorio.png'),
(32, 31, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:943;s:6:\"height\";i:528;s:4:\"file\";s:22:\"2019/08/escritorio.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"escritorio-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"escritorio-300x168.png\";s:5:\"width\";i:300;s:6:\"height\";i:168;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:22:\"escritorio-768x430.png\";s:5:\"width\";i:768;s:6:\"height\";i:430;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(33, 32, '_wp_attached_file', '2019/08/icon.png'),
(34, 32, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:128;s:6:\"height\";i:100;s:4:\"file\";s:16:\"2019/08/icon.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(35, 33, '_wp_attached_file', '2019/08/icon-user.png'),
(36, 33, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:864;s:6:\"height\";i:980;s:4:\"file\";s:21:\"2019/08/icon-user.png\";s:5:\"sizes\";a:3:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"icon-user-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"icon-user-264x300.png\";s:5:\"width\";i:264;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:21:\"icon-user-768x871.png\";s:5:\"width\";i:768;s:6:\"height\";i:871;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(37, 34, '_wp_attached_file', '2019/08/imagem-sala.jpg'),
(38, 34, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:408;s:6:\"height\";i:296;s:4:\"file\";s:23:\"2019/08/imagem-sala.jpg\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"imagem-sala-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:23:\"imagem-sala-300x218.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:218;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(39, 35, '_wp_attached_file', '2019/08/logo-lesto.png'),
(40, 35, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:205;s:6:\"height\";i:63;s:4:\"file\";s:22:\"2019/08/logo-lesto.png\";s:5:\"sizes\";a:1:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"logo-lesto-150x63.png\";s:5:\"width\";i:150;s:6:\"height\";i:63;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(42, 37, '_menu_item_menu_item_parent', '0'),
(43, 37, '_menu_item_object_id', '26'),
(44, 37, '_menu_item_object', 'page'),
(45, 37, '_menu_item_target', ''),
(46, 37, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(47, 37, '_menu_item_xfn', ''),
(48, 37, '_menu_item_url', ''),
(87, 42, '_menu_item_menu_item_parent', '0'),
(50, 38, '_menu_item_type', 'post_type'),
(51, 38, '_menu_item_menu_item_parent', '0'),
(52, 38, '_menu_item_object_id', '24'),
(53, 38, '_menu_item_object', 'page'),
(54, 38, '_menu_item_target', ''),
(55, 38, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(56, 38, '_menu_item_xfn', ''),
(57, 38, '_menu_item_url', ''),
(89, 42, '_menu_item_object', 'page'),
(59, 39, '_menu_item_type', 'post_type'),
(60, 39, '_menu_item_menu_item_parent', '0'),
(61, 39, '_menu_item_object_id', '22'),
(62, 39, '_menu_item_object', 'page'),
(63, 39, '_menu_item_target', ''),
(64, 39, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(65, 39, '_menu_item_xfn', ''),
(66, 39, '_menu_item_url', ''),
(88, 42, '_menu_item_object_id', '26'),
(68, 40, '_menu_item_type', 'post_type'),
(69, 40, '_menu_item_menu_item_parent', '0'),
(70, 40, '_menu_item_object_id', '20'),
(71, 40, '_menu_item_object', 'page'),
(72, 40, '_menu_item_target', ''),
(73, 40, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(74, 40, '_menu_item_xfn', ''),
(75, 40, '_menu_item_url', ''),
(86, 42, '_menu_item_type', 'post_type'),
(77, 41, '_menu_item_type', 'post_type'),
(78, 41, '_menu_item_menu_item_parent', '0'),
(79, 41, '_menu_item_object_id', '18'),
(80, 41, '_menu_item_object', 'page'),
(81, 41, '_menu_item_target', ''),
(82, 41, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(83, 41, '_menu_item_xfn', ''),
(84, 41, '_menu_item_url', ''),
(90, 42, '_menu_item_target', ''),
(91, 42, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(92, 42, '_menu_item_xfn', ''),
(93, 42, '_menu_item_url', ''),
(135, 49, '_menu_item_object', 'custom'),
(95, 43, '_menu_item_type', 'post_type'),
(96, 43, '_menu_item_menu_item_parent', '0'),
(97, 43, '_menu_item_object_id', '24'),
(98, 43, '_menu_item_object', 'page'),
(99, 43, '_menu_item_target', ''),
(100, 43, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(101, 43, '_menu_item_xfn', ''),
(102, 43, '_menu_item_url', ''),
(134, 49, '_menu_item_object_id', '49'),
(104, 44, '_menu_item_type', 'post_type'),
(105, 44, '_menu_item_menu_item_parent', '0'),
(106, 44, '_menu_item_object_id', '22'),
(107, 44, '_menu_item_object', 'page'),
(108, 44, '_menu_item_target', ''),
(109, 44, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(110, 44, '_menu_item_xfn', ''),
(111, 44, '_menu_item_url', ''),
(133, 49, '_menu_item_menu_item_parent', '0'),
(113, 45, '_menu_item_type', 'post_type'),
(114, 45, '_menu_item_menu_item_parent', '0'),
(115, 45, '_menu_item_object_id', '20'),
(116, 45, '_menu_item_object', 'page'),
(117, 45, '_menu_item_target', ''),
(118, 45, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(119, 45, '_menu_item_xfn', ''),
(120, 45, '_menu_item_url', ''),
(132, 49, '_menu_item_type', 'custom'),
(122, 46, '_menu_item_type', 'post_type'),
(123, 46, '_menu_item_menu_item_parent', '0'),
(124, 46, '_menu_item_object_id', '18'),
(125, 46, '_menu_item_object', 'page'),
(126, 46, '_menu_item_target', ''),
(127, 46, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(128, 46, '_menu_item_xfn', ''),
(129, 46, '_menu_item_url', ''),
(131, 47, '_edit_lock', '1565383335:1'),
(136, 49, '_menu_item_target', ''),
(137, 49, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(138, 49, '_menu_item_xfn', ''),
(139, 49, '_menu_item_url', 'http://localhost/projetos/projeto_lesto_site/dicas/'),
(141, 50, '_wp_attached_file', '2019/08/bannersala.png'),
(142, 50, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:470;s:4:\"file\";s:22:\"2019/08/bannersala.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:22:\"bannersala-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:21:\"bannersala-300x73.png\";s:5:\"width\";i:300;s:6:\"height\";i:73;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:22:\"bannersala-768x188.png\";s:5:\"width\";i:768;s:6:\"height\";i:188;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:23:\"bannersala-1024x251.png\";s:5:\"width\";i:1024;s:6:\"height\";i:251;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(143, 22, '_thumbnail_id', '50'),
(144, 51, '_edit_last', '1'),
(145, 51, '_edit_lock', '1565641378:1'),
(146, 51, '_thumbnail_id', '34'),
(147, 51, 'Projeto_preco_clientes_lesto', 'R$14,00/hora'),
(148, 51, 'Projeto_preco_clientes_externos', 'R$20,00/hora'),
(149, 51, 'LestoEscritorios_preco_clientes_lesto', 'R$14,00/hora'),
(150, 51, 'LestoEscritorios_preco_clientes_externos', 'R$20,00/hora'),
(151, 53, '_wp_attached_file', '2019/08/s10.png'),
(152, 53, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:408;s:6:\"height\";i:307;s:4:\"file\";s:15:\"2019/08/s10.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"s10-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"s10-300x226.png\";s:5:\"width\";i:300;s:6:\"height\";i:226;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(153, 54, '_wp_attached_file', '2019/08/s11.png'),
(154, 54, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:408;s:6:\"height\";i:296;s:4:\"file\";s:15:\"2019/08/s11.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"s11-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"s11-300x218.png\";s:5:\"width\";i:300;s:6:\"height\";i:218;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(155, 55, '_wp_attached_file', '2019/08/s12.png'),
(156, 55, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:408;s:6:\"height\";i:296;s:4:\"file\";s:15:\"2019/08/s12.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"s12-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"s12-300x218.png\";s:5:\"width\";i:300;s:6:\"height\";i:218;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(157, 56, '_wp_attached_file', '2019/08/s1.png'),
(158, 56, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:408;s:6:\"height\";i:296;s:4:\"file\";s:14:\"2019/08/s1.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"s1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"s1-300x218.png\";s:5:\"width\";i:300;s:6:\"height\";i:218;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(159, 57, '_wp_attached_file', '2019/08/s2.png'),
(160, 57, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:408;s:6:\"height\";i:296;s:4:\"file\";s:14:\"2019/08/s2.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"s2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"s2-300x218.png\";s:5:\"width\";i:300;s:6:\"height\";i:218;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(161, 58, '_wp_attached_file', '2019/08/s3.png'),
(162, 58, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:408;s:6:\"height\";i:296;s:4:\"file\";s:14:\"2019/08/s3.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"s3-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"s3-300x218.png\";s:5:\"width\";i:300;s:6:\"height\";i:218;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(163, 59, '_wp_attached_file', '2019/08/s4.png'),
(164, 59, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:408;s:6:\"height\";i:301;s:4:\"file\";s:14:\"2019/08/s4.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"s4-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"s4-300x221.png\";s:5:\"width\";i:300;s:6:\"height\";i:221;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(165, 60, '_wp_attached_file', '2019/08/s5.png'),
(166, 60, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:408;s:6:\"height\";i:296;s:4:\"file\";s:14:\"2019/08/s5.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"s5-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"s5-300x218.png\";s:5:\"width\";i:300;s:6:\"height\";i:218;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(167, 61, '_wp_attached_file', '2019/08/s6.png'),
(168, 61, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:408;s:6:\"height\";i:296;s:4:\"file\";s:14:\"2019/08/s6.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"s6-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"s6-300x218.png\";s:5:\"width\";i:300;s:6:\"height\";i:218;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(169, 62, '_wp_attached_file', '2019/08/s7.png'),
(170, 62, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:408;s:6:\"height\";i:299;s:4:\"file\";s:14:\"2019/08/s7.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"s7-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"s7-300x220.png\";s:5:\"width\";i:300;s:6:\"height\";i:220;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(171, 63, '_wp_attached_file', '2019/08/s8.png'),
(172, 63, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:408;s:6:\"height\";i:296;s:4:\"file\";s:14:\"2019/08/s8.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"s8-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"s8-300x218.png\";s:5:\"width\";i:300;s:6:\"height\";i:218;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(173, 64, '_wp_attached_file', '2019/08/s9.png'),
(174, 64, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:408;s:6:\"height\";i:296;s:4:\"file\";s:14:\"2019/08/s9.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"s9-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"s9-300x218.png\";s:5:\"width\";i:300;s:6:\"height\";i:218;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(175, 66, '_edit_last', '1'),
(176, 66, '_edit_lock', '1565641298:1'),
(177, 66, '_thumbnail_id', '57'),
(178, 66, 'LestoEscritorios_preco_clientes_lesto', 'R$14,00/hora'),
(179, 66, 'LestoEscritorios_preco_clientes_externos', 'R$20,00/hora'),
(180, 67, '_edit_last', '1'),
(181, 67, '_edit_lock', '1565641335:1'),
(182, 67, '_thumbnail_id', '58'),
(183, 67, 'LestoEscritorios_preco_clientes_lesto', 'R$17,50/hora'),
(184, 67, 'LestoEscritorios_preco_clientes_externos', 'R$25,00/hora'),
(185, 68, '_edit_last', '1'),
(186, 68, '_edit_lock', '1565641375:1'),
(187, 68, '_thumbnail_id', '59'),
(188, 68, 'LestoEscritorios_preco_clientes_lesto', 'R$15,50/hora'),
(189, 68, 'LestoEscritorios_preco_clientes_externos', 'R$25,00/hora'),
(190, 69, '_edit_last', '1'),
(191, 69, '_edit_lock', '1565641403:1'),
(192, 69, '_thumbnail_id', '60'),
(193, 69, 'LestoEscritorios_preco_clientes_lesto', 'R$24,00/hora'),
(194, 69, 'LestoEscritorios_preco_clientes_externos', 'R$35,00/hora'),
(195, 70, '_edit_last', '1'),
(196, 70, '_edit_lock', '1565641426:1'),
(197, 70, '_thumbnail_id', '61'),
(198, 70, 'LestoEscritorios_preco_clientes_lesto', 'R$28,00/hora'),
(199, 70, 'LestoEscritorios_preco_clientes_externos', 'R$40,00/hora'),
(200, 71, '_edit_last', '1'),
(201, 71, '_edit_lock', '1565641478:1'),
(202, 71, '_thumbnail_id', '62'),
(203, 71, 'LestoEscritorios_preco_clientes_lesto', 'R$28,00/hora'),
(204, 71, 'LestoEscritorios_preco_clientes_externos', 'R$40,00/hora'),
(205, 72, '_edit_last', '1'),
(206, 72, '_edit_lock', '1565641508:1'),
(207, 72, '_thumbnail_id', '63'),
(208, 72, 'LestoEscritorios_preco_clientes_lesto', 'R$35,00/hora'),
(209, 72, 'LestoEscritorios_preco_clientes_externos', 'R$50,00/hora'),
(210, 73, '_edit_last', '1'),
(211, 73, '_edit_lock', '1565641529:1'),
(212, 73, '_thumbnail_id', '64'),
(213, 73, 'LestoEscritorios_preco_clientes_lesto', 'R$35,50/hora'),
(214, 73, 'LestoEscritorios_preco_clientes_externos', 'R$50,00/hora'),
(215, 74, '_edit_last', '1'),
(216, 74, '_edit_lock', '1565641556:1'),
(217, 74, '_thumbnail_id', '53'),
(218, 74, 'LestoEscritorios_preco_clientes_lesto', 'R$15,50/hora'),
(219, 74, 'LestoEscritorios_preco_clientes_externos', 'R$25,00/hora'),
(220, 75, '_edit_last', '1'),
(221, 75, '_edit_lock', '1565641572:1'),
(222, 75, '_thumbnail_id', '54'),
(223, 75, 'LestoEscritorios_preco_clientes_lesto', 'R$24,00/hora'),
(224, 75, 'LestoEscritorios_preco_clientes_externos', 'R$35,00/hora'),
(225, 76, '_edit_last', '1'),
(226, 76, '_edit_lock', '1565641887:1'),
(227, 76, '_thumbnail_id', '55'),
(228, 76, 'LestoEscritorios_preco_clientes_lesto', 'R$28,00/hora'),
(229, 76, 'LestoEscritorios_preco_clientes_externos', 'R$40,00/hora'),
(230, 78, '_wp_attached_file', '2019/08/d9.png'),
(231, 78, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:467;s:6:\"height\";i:262;s:4:\"file\";s:14:\"2019/08/d9.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"d9-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"d9-300x168.png\";s:5:\"width\";i:300;s:6:\"height\";i:168;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(232, 79, '_wp_attached_file', '2019/08/bannerdicas.png'),
(233, 79, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:470;s:4:\"file\";s:23:\"2019/08/bannerdicas.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:23:\"bannerdicas-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:22:\"bannerdicas-300x73.png\";s:5:\"width\";i:300;s:6:\"height\";i:73;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:23:\"bannerdicas-768x188.png\";s:5:\"width\";i:768;s:6:\"height\";i:188;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:24:\"bannerdicas-1024x251.png\";s:5:\"width\";i:1024;s:6:\"height\";i:251;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(234, 80, '_wp_attached_file', '2019/08/d2.png'),
(235, 80, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:467;s:6:\"height\";i:262;s:4:\"file\";s:14:\"2019/08/d2.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"d2-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"d2-300x168.png\";s:5:\"width\";i:300;s:6:\"height\";i:168;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(236, 81, '_wp_attached_file', '2019/08/d3.png'),
(237, 81, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:467;s:6:\"height\";i:274;s:4:\"file\";s:14:\"2019/08/d3.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"d3-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"d3-300x176.png\";s:5:\"width\";i:300;s:6:\"height\";i:176;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(238, 82, '_wp_attached_file', '2019/08/d4.png'),
(239, 82, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:467;s:6:\"height\";i:262;s:4:\"file\";s:14:\"2019/08/d4.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"d4-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"d4-300x168.png\";s:5:\"width\";i:300;s:6:\"height\";i:168;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(240, 83, '_wp_attached_file', '2019/08/d5.png'),
(241, 83, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:467;s:6:\"height\";i:262;s:4:\"file\";s:14:\"2019/08/d5.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"d5-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"d5-300x168.png\";s:5:\"width\";i:300;s:6:\"height\";i:168;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(242, 84, '_wp_attached_file', '2019/08/d6.png'),
(243, 84, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:467;s:6:\"height\";i:262;s:4:\"file\";s:14:\"2019/08/d6.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"d6-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"d6-300x168.png\";s:5:\"width\";i:300;s:6:\"height\";i:168;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(244, 85, '_wp_attached_file', '2019/08/d7.png'),
(245, 85, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:467;s:6:\"height\";i:262;s:4:\"file\";s:14:\"2019/08/d7.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"d7-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"d7-300x168.png\";s:5:\"width\";i:300;s:6:\"height\";i:168;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(246, 86, '_wp_attached_file', '2019/08/d8.png'),
(247, 86, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:467;s:6:\"height\";i:262;s:4:\"file\";s:14:\"2019/08/d8.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"d8-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"d8-300x168.png\";s:5:\"width\";i:300;s:6:\"height\";i:168;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(248, 1, '_wp_trash_meta_status', 'publish'),
(249, 1, '_wp_trash_meta_time', '1565644100'),
(250, 1, '_wp_desired_post_slug', 'ola-mundo'),
(251, 1, '_wp_trash_meta_comments_status', 'a:1:{i:1;s:1:\"1\";}'),
(252, 88, '_edit_lock', '1565645586:1'),
(253, 89, '_wp_attached_file', '2019/08/d1.png'),
(254, 89, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:467;s:6:\"height\";i:262;s:4:\"file\";s:14:\"2019/08/d1.png\";s:5:\"sizes\";a:2:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"d1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"d1-300x168.png\";s:5:\"width\";i:300;s:6:\"height\";i:168;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(259, 95, '_edit_lock', '1565715837:1'),
(256, 88, '_thumbnail_id', '89'),
(262, 97, '_edit_lock', '1565715869:1'),
(261, 95, '_thumbnail_id', '80'),
(265, 99, '_edit_lock', '1565715915:1'),
(264, 97, '_thumbnail_id', '81'),
(268, 101, '_edit_lock', '1565717867:1'),
(267, 99, '_thumbnail_id', '82'),
(271, 103, '_edit_lock', '1565715993:1'),
(270, 101, '_thumbnail_id', '83'),
(274, 105, '_edit_lock', '1565716139:1'),
(273, 103, '_thumbnail_id', '84'),
(275, 106, '_edit_lock', '1565716012:1'),
(278, 108, '_edit_lock', '1565716035:1'),
(277, 106, '_thumbnail_id', '85'),
(281, 110, '_edit_lock', '1565716064:1'),
(280, 108, '_thumbnail_id', '86'),
(283, 110, '_thumbnail_id', '78');

-- --------------------------------------------------------

--
-- Estrutura da tabela `le_posts`
--

DROP TABLE IF EXISTS `le_posts`;
CREATE TABLE IF NOT EXISTS `le_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=MyISAM AUTO_INCREMENT=112 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `le_posts`
--

INSERT INTO `le_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2019-08-08 16:19:51', '2019-08-08 19:19:51', '<!-- wp:paragraph -->\n<p>Boas-vindas ao WordPress. Esse é o seu primeiro post. Edite-o ou exclua-o, e então comece a escrever!</p>\n<!-- /wp:paragraph -->', 'Olá, mundo!', '', 'trash', 'open', 'open', '', 'ola-mundo__trashed', '', '', '2019-08-12 18:08:20', '2019-08-12 21:08:20', '', 0, 'http://localhost/projetos/projeto_lesto_site/?p=1', 0, 'post', '', 1),
(2, 1, '2019-08-08 16:19:51', '2019-08-08 19:19:51', '<!-- wp:paragraph -->\n<p>Esta é uma página de exemplo. É diferente de um post no blog porque ela permanecerá em um lugar e aparecerá na navegação do seu site na maioria dos temas. Muitas pessoas começam com uma página que as apresenta a possíveis visitantes do site. Ela pode dizer algo assim:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Olá! Eu sou um mensageiro de bicicleta durante o dia, ator aspirante à noite, e este é o meu site. Eu moro em São Paulo, tenho um grande cachorro chamado Rex e gosto de tomar caipirinha (e banhos de chuva).</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...ou alguma coisa assim:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>A Companhia de Miniaturas XYZ foi fundada em 1971, e desde então tem fornecido miniaturas de qualidade ao público. Localizada na cidade de Itu, a XYZ emprega mais de 2.000 pessoas e faz coisas grandiosas para a comunidade da cidade.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>Como um novo usuário do WordPress, você deveria ir ao <a href=\"http://localhost/projetos/projeto_lesto_site/wp-admin/\">painel</a> para excluir essa página e criar novas páginas para o seu conteúdo. Divirta-se!</p>\n<!-- /wp:paragraph -->', 'Página de exemplo', '', 'trash', 'closed', 'open', '', 'pagina-exemplo__trashed', '', '', '2019-08-08 18:14:27', '2019-08-08 21:14:27', '', 0, 'http://localhost/projetos/projeto_lesto_site/?page_id=2', 0, 'page', '', 0),
(3, 1, '2019-08-08 16:19:51', '2019-08-08 19:19:51', '<!-- wp:heading --><h2>Quem somos</h2><!-- /wp:heading --><!-- wp:paragraph --><p>O endereço do nosso site é: http://localhost/projetos/projeto_lesto_site.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Quais dados pessoais coletamos e porque</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comentários</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Quando os visitantes deixam comentários no site, coletamos os dados mostrados no formulário de comentários, além do endereço de IP e de dados do navegador do visitante, para auxiliar na detecção de spam.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Uma sequência anonimizada de caracteres criada a partir do seu e-mail (também chamada de hash) poderá ser enviada para o Gravatar para verificar se você usa o serviço. A política de privacidade do Gravatar está disponível aqui: https://automattic.com/privacy/. Depois da aprovação do seu comentário, a foto do seu perfil fica visível publicamente junto de seu comentário.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Mídia</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Se você envia imagens para o site, evite enviar as que contenham dados de localização incorporados (EXIF GPS). Visitantes podem baixar estas imagens do site e extrair delas seus dados de localização.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Formulários de contato</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Ao deixar um comentário no site, você poderá optar por salvar seu nome, e-mail e site nos cookies. Isso visa seu conforto, assim você não precisará preencher seus  dados novamente quando fizer outro comentário. Estes cookies duram um ano.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Se você tem uma conta e acessa este site, um cookie temporário será criado para determinar se seu navegador aceita cookies. Ele não contém nenhum dado pessoal e será descartado quando você fechar seu navegador.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Quando você acessa sua conta no site, também criamos vários cookies para salvar os dados da sua conta e suas escolhas de exibição de tela. Cookies de login são mantidos por dois dias e cookies de opções de tela por um ano. Se você selecionar &quot;Lembrar-me&quot;, seu acesso será mantido por duas semanas. Se você se desconectar da sua conta, os cookies de login serão removidos.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Se você editar ou publicar um artigo, um cookie adicional será salvo no seu navegador. Este cookie não inclui nenhum dado pessoal e simplesmente indica o ID do post referente ao artigo que você acabou de editar. Ele expira depois de 1 dia.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Mídia incorporada de outros sites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Artigos neste site podem incluir conteúdo incorporado como, por exemplo, vídeos, imagens, artigos, etc. Conteúdos incorporados de outros sites se comportam exatamente da mesma forma como se o visitante estivesse visitando o outro site.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Estes sites podem coletar dados sobre você, usar cookies, incorporar rastreamento adicional de terceiros e monitorar sua interação com este conteúdo incorporado, incluindo sua interação com o conteúdo incorporado se você tem uma conta e está conectado com o site.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Análises</h3><!-- /wp:heading --><!-- wp:heading --><h2>Com quem partilhamos seus dados</h2><!-- /wp:heading --><!-- wp:heading --><h2>Por quanto tempo mantemos os seus dados</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Se você deixar um comentário, o comentário e os seus metadados são conservados indefinidamente. Fazemos isso para que seja possível reconhecer e aprovar automaticamente qualquer comentário posterior ao invés de retê-lo para moderação.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Para usuários que se registram no nosso site (se houver), também guardamos as informações pessoais que fornecem no seu perfil de usuário. Todos os usuários podem ver, editar ou excluir suas informações pessoais a qualquer momento (só não é possível alterar o seu username). Os administradores de sites também podem ver e editar estas informações.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Quais os seus direitos sobre seus dados</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Se você tiver uma conta neste site ou se tiver deixado comentários, pode solicitar um arquivo exportado dos dados pessoais que mantemos sobre você, inclusive quaisquer dados que nos tenha fornecido. Também pode solicitar que removamos qualquer dado pessoal que mantemos sobre você. Isto não inclui nenhuns dados que somos obrigados a manter para propósitos administrativos, legais ou de segurança.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Para onde enviamos seus dados</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Comentários de visitantes podem ser marcados por um serviço automático de detecção de spam.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Suas informações de contato</h2><!-- /wp:heading --><!-- wp:heading --><h2>Informações adicionais</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Como protegemos seus dados</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Quais são nossos procedimentos contra violação de dados</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>De quais terceiros nós recebemos dados</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Quais tomadas de decisão ou análises de perfil automatizadas fazemos com os dados de usuários</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Requisitos obrigatórios de divulgação para sua categoria profissional</h3><!-- /wp:heading -->', 'Política de privacidade', '', 'trash', 'closed', 'open', '', 'politica-de-privacidade__trashed', '', '', '2019-08-08 18:14:27', '2019-08-08 21:14:27', '', 0, 'http://localhost/projetos/projeto_lesto_site/?page_id=3', 0, 'page', '', 0),
(4, 1, '2019-08-08 16:20:51', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-08-08 16:20:51', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/projeto_lesto_site/?p=4', 0, 'post', '', 0),
(5, 1, '2019-08-08 17:04:18', '2019-08-08 20:04:18', '<div class=\"info\">\r\n<label for=\"nome\">Seu Nome</label>\r\n[text* nome id:nome class:nome placeholder \"Nome\"]\r\n</div>\r\n<div  class=\"info\">\r\n<label for=\"email\">Seu e-email</label>\r\n[email* email id:email class:email placeholder \"Seu e-mail\"]\r\n</div>\r\n<div  class=\"info full\">\r\n<label for=\"assunto\">Assunto</label>\r\n[text* assunto id:assunto class:assunto placeholder \"Assunto\"]\r\n</div>\r\n<div  class=\"info full\">\r\n<label for=\"mensagem\" >Mensagem</label>\r\n[textarea* mensagem id:mensagem class:mensagem placeholder \"Mesagem\"]\r\n</div>\r\n<div class=\"btn-enviar\">\r\n[submit id:Enviar class:Enviar \"Enviar\"]\r\n</div>\n1\nLesto escritórios \"[your-subject]\"\nLesto escritórios <devhcdesenvolvimentos@gmail.com>\ndevhcdesenvolvimentos@gmail.com\nDe: [nome]\r\nEmail: [email]\r\nAssunto: [assunto]\r\n\r\nMensagem:\r\n[mensagem]\r\n-- \r\nThis e-mail was sent from a contact form on Lesto escritórios (http://localhost/projetos/projeto_lesto_site)\n\n\n\n\n\nLesto escritórios \"[your-subject]\"\nLesto escritórios <devhcdesenvolvimentos@gmail.com>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Lesto escritórios (http://localhost/projetos/projeto_lesto_site)\nReply-To: devhcdesenvolvimentos@gmail.com\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nYour entered code is incorrect.\nThe e-mail address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.', 'Fomulário de contato', '', 'publish', 'closed', 'closed', '', 'contact-form-1', '', '', '2019-08-09 18:14:47', '2019-08-09 21:14:47', '', 0, 'http://localhost/projetos/projeto_lesto_site/?post_type=wpcf7_contact_form&#038;p=5', 0, 'wpcf7_contact_form', '', 0),
(6, 1, '2019-08-08 17:39:47', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-08-08 17:39:47', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/projeto_lesto_site/?post_type=plano&p=6', 0, 'plano', '', 0),
(7, 1, '2019-08-08 17:40:29', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-08-08 17:40:29', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/projeto_lesto_site/?post_type=plano&p=7', 0, 'plano', '', 0),
(8, 1, '2019-08-08 17:44:38', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-08-08 17:44:38', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/projeto_lesto_site/?post_type=plano&p=8', 0, 'plano', '', 0),
(9, 1, '2019-08-08 17:50:58', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-08-08 17:50:58', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/projeto_lesto_site/?post_type=unidade&p=9', 0, 'unidade', '', 0),
(10, 1, '2019-08-08 17:52:15', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-08-08 17:52:15', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/projeto_lesto_site/?post_type=unidade&p=10', 0, 'unidade', '', 0),
(11, 1, '2019-08-08 17:52:20', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-08-08 17:52:20', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/projeto_lesto_site/?post_type=destaque&p=11', 0, 'destaque', '', 0),
(12, 1, '2019-08-08 17:52:32', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-08-08 17:52:32', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/projeto_lesto_site/?post_type=destaque&p=12', 0, 'destaque', '', 0),
(13, 1, '2019-08-08 17:52:58', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-08-08 17:52:58', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/projeto_lesto_site/?post_type=destaque&p=13', 0, 'destaque', '', 0),
(14, 1, '2019-08-08 17:53:28', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-08-08 17:53:28', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/projeto_lesto_site/?post_type=destaque&p=14', 0, 'destaque', '', 0),
(15, 1, '2019-08-08 18:06:09', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-08-08 18:06:09', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/projeto_lesto_site/?post_type=destaque&p=15', 0, 'destaque', '', 0),
(16, 1, '2019-08-08 18:14:27', '2019-08-08 21:14:27', '<!-- wp:heading --><h2>Quem somos</h2><!-- /wp:heading --><!-- wp:paragraph --><p>O endereço do nosso site é: http://localhost/projetos/projeto_lesto_site.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Quais dados pessoais coletamos e porque</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comentários</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Quando os visitantes deixam comentários no site, coletamos os dados mostrados no formulário de comentários, além do endereço de IP e de dados do navegador do visitante, para auxiliar na detecção de spam.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Uma sequência anonimizada de caracteres criada a partir do seu e-mail (também chamada de hash) poderá ser enviada para o Gravatar para verificar se você usa o serviço. A política de privacidade do Gravatar está disponível aqui: https://automattic.com/privacy/. Depois da aprovação do seu comentário, a foto do seu perfil fica visível publicamente junto de seu comentário.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Mídia</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Se você envia imagens para o site, evite enviar as que contenham dados de localização incorporados (EXIF GPS). Visitantes podem baixar estas imagens do site e extrair delas seus dados de localização.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Formulários de contato</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Ao deixar um comentário no site, você poderá optar por salvar seu nome, e-mail e site nos cookies. Isso visa seu conforto, assim você não precisará preencher seus  dados novamente quando fizer outro comentário. Estes cookies duram um ano.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Se você tem uma conta e acessa este site, um cookie temporário será criado para determinar se seu navegador aceita cookies. Ele não contém nenhum dado pessoal e será descartado quando você fechar seu navegador.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Quando você acessa sua conta no site, também criamos vários cookies para salvar os dados da sua conta e suas escolhas de exibição de tela. Cookies de login são mantidos por dois dias e cookies de opções de tela por um ano. Se você selecionar &quot;Lembrar-me&quot;, seu acesso será mantido por duas semanas. Se você se desconectar da sua conta, os cookies de login serão removidos.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Se você editar ou publicar um artigo, um cookie adicional será salvo no seu navegador. Este cookie não inclui nenhum dado pessoal e simplesmente indica o ID do post referente ao artigo que você acabou de editar. Ele expira depois de 1 dia.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Mídia incorporada de outros sites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Artigos neste site podem incluir conteúdo incorporado como, por exemplo, vídeos, imagens, artigos, etc. Conteúdos incorporados de outros sites se comportam exatamente da mesma forma como se o visitante estivesse visitando o outro site.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Estes sites podem coletar dados sobre você, usar cookies, incorporar rastreamento adicional de terceiros e monitorar sua interação com este conteúdo incorporado, incluindo sua interação com o conteúdo incorporado se você tem uma conta e está conectado com o site.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Análises</h3><!-- /wp:heading --><!-- wp:heading --><h2>Com quem partilhamos seus dados</h2><!-- /wp:heading --><!-- wp:heading --><h2>Por quanto tempo mantemos os seus dados</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Se você deixar um comentário, o comentário e os seus metadados são conservados indefinidamente. Fazemos isso para que seja possível reconhecer e aprovar automaticamente qualquer comentário posterior ao invés de retê-lo para moderação.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Para usuários que se registram no nosso site (se houver), também guardamos as informações pessoais que fornecem no seu perfil de usuário. Todos os usuários podem ver, editar ou excluir suas informações pessoais a qualquer momento (só não é possível alterar o seu username). Os administradores de sites também podem ver e editar estas informações.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Quais os seus direitos sobre seus dados</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Se você tiver uma conta neste site ou se tiver deixado comentários, pode solicitar um arquivo exportado dos dados pessoais que mantemos sobre você, inclusive quaisquer dados que nos tenha fornecido. Também pode solicitar que removamos qualquer dado pessoal que mantemos sobre você. Isto não inclui nenhuns dados que somos obrigados a manter para propósitos administrativos, legais ou de segurança.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Para onde enviamos seus dados</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Comentários de visitantes podem ser marcados por um serviço automático de detecção de spam.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Suas informações de contato</h2><!-- /wp:heading --><!-- wp:heading --><h2>Informações adicionais</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Como protegemos seus dados</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Quais são nossos procedimentos contra violação de dados</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>De quais terceiros nós recebemos dados</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Quais tomadas de decisão ou análises de perfil automatizadas fazemos com os dados de usuários</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Requisitos obrigatórios de divulgação para sua categoria profissional</h3><!-- /wp:heading -->', 'Política de privacidade', '', 'inherit', 'closed', 'closed', '', '3-revision-v1', '', '', '2019-08-08 18:14:27', '2019-08-08 21:14:27', '', 3, 'http://localhost/projetos/projeto_lesto_site/2019/08/08/3-revision-v1/', 0, 'revision', '', 0),
(17, 1, '2019-08-08 18:14:27', '2019-08-08 21:14:27', '<!-- wp:paragraph -->\n<p>Esta é uma página de exemplo. É diferente de um post no blog porque ela permanecerá em um lugar e aparecerá na navegação do seu site na maioria dos temas. Muitas pessoas começam com uma página que as apresenta a possíveis visitantes do site. Ela pode dizer algo assim:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Olá! Eu sou um mensageiro de bicicleta durante o dia, ator aspirante à noite, e este é o meu site. Eu moro em São Paulo, tenho um grande cachorro chamado Rex e gosto de tomar caipirinha (e banhos de chuva).</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...ou alguma coisa assim:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>A Companhia de Miniaturas XYZ foi fundada em 1971, e desde então tem fornecido miniaturas de qualidade ao público. Localizada na cidade de Itu, a XYZ emprega mais de 2.000 pessoas e faz coisas grandiosas para a comunidade da cidade.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>Como um novo usuário do WordPress, você deveria ir ao <a href=\"http://localhost/projetos/projeto_lesto_site/wp-admin/\">painel</a> para excluir essa página e criar novas páginas para o seu conteúdo. Divirta-se!</p>\n<!-- /wp:paragraph -->', 'Página de exemplo', '', 'inherit', 'closed', 'closed', '', '2-revision-v1', '', '', '2019-08-08 18:14:27', '2019-08-08 21:14:27', '', 2, 'http://localhost/projetos/projeto_lesto_site/2019/08/08/2-revision-v1/', 0, 'revision', '', 0),
(18, 1, '2019-08-08 18:14:46', '2019-08-08 21:14:46', '', 'Inicial', '', 'publish', 'closed', 'closed', '', 'inicial', '', '', '2019-08-08 18:14:46', '2019-08-08 21:14:46', '', 0, 'http://localhost/projetos/projeto_lesto_site/?page_id=18', 0, 'page', '', 0),
(19, 1, '2019-08-08 18:14:46', '2019-08-08 21:14:46', '', 'Inicial', '', 'inherit', 'closed', 'closed', '', '18-revision-v1', '', '', '2019-08-08 18:14:46', '2019-08-08 21:14:46', '', 18, 'http://localhost/projetos/projeto_lesto_site/2019/08/08/18-revision-v1/', 0, 'revision', '', 0),
(20, 1, '2019-08-08 18:15:04', '2019-08-08 21:15:04', '', 'Contato', '', 'publish', 'closed', 'closed', '', 'contato', '', '', '2019-08-08 18:15:04', '2019-08-08 21:15:04', '', 0, 'http://localhost/projetos/projeto_lesto_site/?page_id=20', 0, 'page', '', 0),
(21, 1, '2019-08-08 18:15:04', '2019-08-08 21:15:04', '', 'Contato', '', 'inherit', 'closed', 'closed', '', '20-revision-v1', '', '', '2019-08-08 18:15:04', '2019-08-08 21:15:04', '', 20, 'http://localhost/projetos/projeto_lesto_site/2019/08/08/20-revision-v1/', 0, 'revision', '', 0),
(22, 1, '2019-08-08 18:15:16', '2019-08-08 21:15:16', '', 'Salas', '', 'publish', 'closed', 'closed', '', 'salas', '', '', '2019-08-12 17:52:34', '2019-08-12 20:52:34', '', 0, 'http://localhost/projetos/projeto_lesto_site/?page_id=22', 0, 'page', '', 0),
(23, 1, '2019-08-08 18:15:16', '2019-08-08 21:15:16', '', 'Salas', '', 'inherit', 'closed', 'closed', '', '22-revision-v1', '', '', '2019-08-08 18:15:16', '2019-08-08 21:15:16', '', 22, 'http://localhost/projetos/projeto_lesto_site/2019/08/08/22-revision-v1/', 0, 'revision', '', 0),
(24, 1, '2019-08-08 18:15:28', '2019-08-08 21:15:28', '', 'Planos', '', 'publish', 'closed', 'closed', '', 'planos', '', '', '2019-08-08 18:15:28', '2019-08-08 21:15:28', '', 0, 'http://localhost/projetos/projeto_lesto_site/?page_id=24', 0, 'page', '', 0),
(25, 1, '2019-08-08 18:15:28', '2019-08-08 21:15:28', '', 'Planos', '', 'inherit', 'closed', 'closed', '', '24-revision-v1', '', '', '2019-08-08 18:15:28', '2019-08-08 21:15:28', '', 24, 'http://localhost/projetos/projeto_lesto_site/2019/08/08/24-revision-v1/', 0, 'revision', '', 0),
(26, 1, '2019-08-08 18:15:45', '2019-08-08 21:15:45', '', 'Localização', '', 'publish', 'closed', 'closed', '', 'localizacao', '', '', '2019-08-09 17:25:01', '2019-08-09 20:25:01', '', 0, 'http://localhost/projetos/projeto_lesto_site/?page_id=26', 0, 'page', '', 0),
(27, 1, '2019-08-08 18:15:45', '2019-08-08 21:15:45', '', 'Unidades', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2019-08-08 18:15:45', '2019-08-08 21:15:45', '', 26, 'http://localhost/projetos/projeto_lesto_site/2019/08/08/26-revision-v1/', 0, 'revision', '', 0),
(28, 1, '2019-08-09 17:21:07', '2019-08-09 20:21:07', '', 'bannerTopoHome', '', 'inherit', 'open', 'closed', '', 'bannertopohome', '', '', '2019-08-09 17:21:07', '2019-08-09 20:21:07', '', 0, 'http://localhost/projetos/projeto_lesto_site/wp-content/uploads/2019/08/bannerTopoHome.jpg', 0, 'attachment', 'image/jpeg', 0),
(29, 1, '2019-08-09 17:21:10', '2019-08-09 20:21:10', '', 'carrossel', '', 'inherit', 'open', 'closed', '', 'carrossel', '', '', '2019-08-09 17:21:10', '2019-08-09 20:21:10', '', 0, 'http://localhost/projetos/projeto_lesto_site/wp-content/uploads/2019/08/carrossel.png', 0, 'attachment', 'image/png', 0),
(36, 1, '2019-08-09 17:24:11', '2019-08-09 20:24:11', '', 'Localização', '', 'inherit', 'closed', 'closed', '', '26-revision-v1', '', '', '2019-08-09 17:24:11', '2019-08-09 20:24:11', '', 26, 'http://localhost/projetos/projeto_lesto_site/26-revision-v1/', 0, 'revision', '', 0),
(31, 1, '2019-08-09 17:21:11', '2019-08-09 20:21:11', '', 'escritorio', '', 'inherit', 'open', 'closed', '', 'escritorio', '', '', '2019-08-09 17:21:11', '2019-08-09 20:21:11', '', 0, 'http://localhost/projetos/projeto_lesto_site/wp-content/uploads/2019/08/escritorio.png', 0, 'attachment', 'image/png', 0),
(32, 1, '2019-08-09 17:21:12', '2019-08-09 20:21:12', '', 'icon', '', 'inherit', 'open', 'closed', '', 'icon', '', '', '2019-08-09 17:21:12', '2019-08-09 20:21:12', '', 0, 'http://localhost/projetos/projeto_lesto_site/wp-content/uploads/2019/08/icon.png', 0, 'attachment', 'image/png', 0),
(33, 1, '2019-08-09 17:21:12', '2019-08-09 20:21:12', '', 'icon-user', '', 'inherit', 'open', 'closed', '', 'icon-user', '', '', '2019-08-09 17:21:12', '2019-08-09 20:21:12', '', 0, 'http://localhost/projetos/projeto_lesto_site/wp-content/uploads/2019/08/icon-user.png', 0, 'attachment', 'image/png', 0),
(34, 1, '2019-08-09 17:21:13', '2019-08-09 20:21:13', '', 'imagem-sala', '', 'inherit', 'open', 'closed', '', 'imagem-sala', '', '', '2019-08-09 17:21:13', '2019-08-09 20:21:13', '', 0, 'http://localhost/projetos/projeto_lesto_site/wp-content/uploads/2019/08/imagem-sala.jpg', 0, 'attachment', 'image/jpeg', 0),
(35, 1, '2019-08-09 17:21:13', '2019-08-09 20:21:13', '', 'logo-lesto', '', 'inherit', 'open', 'closed', '', 'logo-lesto', '', '', '2019-08-09 17:21:13', '2019-08-09 20:21:13', '', 0, 'http://localhost/projetos/projeto_lesto_site/wp-content/uploads/2019/08/logo-lesto.png', 0, 'attachment', 'image/png', 0),
(37, 1, '2019-08-09 17:28:13', '2019-08-09 20:28:13', '', 'Unidade', '', 'publish', 'closed', 'closed', '', 'unidade', '', '', '2019-08-09 17:28:20', '2019-08-09 20:28:20', '', 0, 'http://localhost/projetos/projeto_lesto_site/?p=37', 4, 'nav_menu_item', '', 0),
(38, 1, '2019-08-09 17:28:13', '2019-08-09 20:28:13', ' ', '', '', 'publish', 'closed', 'closed', '', '38', '', '', '2019-08-09 17:28:20', '2019-08-09 20:28:20', '', 0, 'http://localhost/projetos/projeto_lesto_site/?p=38', 2, 'nav_menu_item', '', 0),
(39, 1, '2019-08-09 17:28:13', '2019-08-09 20:28:13', ' ', '', '', 'publish', 'closed', 'closed', '', '39', '', '', '2019-08-09 17:28:20', '2019-08-09 20:28:20', '', 0, 'http://localhost/projetos/projeto_lesto_site/?p=39', 3, 'nav_menu_item', '', 0),
(40, 1, '2019-08-09 17:28:13', '2019-08-09 20:28:13', ' ', '', '', 'publish', 'closed', 'closed', '', '40', '', '', '2019-08-09 17:28:20', '2019-08-09 20:28:20', '', 0, 'http://localhost/projetos/projeto_lesto_site/?p=40', 5, 'nav_menu_item', '', 0),
(41, 1, '2019-08-09 17:28:13', '2019-08-09 20:28:13', ' ', '', '', 'publish', 'closed', 'closed', '', '41', '', '', '2019-08-09 17:28:20', '2019-08-09 20:28:20', '', 0, 'http://localhost/projetos/projeto_lesto_site/?p=41', 1, 'nav_menu_item', '', 0),
(42, 1, '2019-08-09 17:43:09', '2019-08-09 20:43:09', ' ', '', '', 'publish', 'closed', 'closed', '', '42', '', '', '2019-08-09 17:45:16', '2019-08-09 20:45:16', '', 0, 'http://localhost/projetos/projeto_lesto_site/?p=42', 4, 'nav_menu_item', '', 0),
(43, 1, '2019-08-09 17:43:09', '2019-08-09 20:43:09', ' ', '', '', 'publish', 'closed', 'closed', '', '43', '', '', '2019-08-09 17:45:16', '2019-08-09 20:45:16', '', 0, 'http://localhost/projetos/projeto_lesto_site/?p=43', 2, 'nav_menu_item', '', 0),
(44, 1, '2019-08-09 17:43:09', '2019-08-09 20:43:09', ' ', '', '', 'publish', 'closed', 'closed', '', '44', '', '', '2019-08-09 17:45:16', '2019-08-09 20:45:16', '', 0, 'http://localhost/projetos/projeto_lesto_site/?p=44', 3, 'nav_menu_item', '', 0),
(45, 1, '2019-08-09 17:43:09', '2019-08-09 20:43:09', ' ', '', '', 'publish', 'closed', 'closed', '', '45', '', '', '2019-08-09 17:45:16', '2019-08-09 20:45:16', '', 0, 'http://localhost/projetos/projeto_lesto_site/?p=45', 5, 'nav_menu_item', '', 0),
(46, 1, '2019-08-09 17:43:09', '2019-08-09 20:43:09', ' ', '', '', 'publish', 'closed', 'closed', '', '46', '', '', '2019-08-09 17:45:16', '2019-08-09 20:45:16', '', 0, 'http://localhost/projetos/projeto_lesto_site/?p=46', 1, 'nav_menu_item', '', 0),
(47, 1, '2019-08-09 17:43:44', '2019-08-09 20:43:44', '', 'Dicas', '', 'publish', 'closed', 'closed', '', 'dicas', '', '', '2019-08-09 17:43:44', '2019-08-09 20:43:44', '', 0, 'http://localhost/projetos/projeto_lesto_site/?page_id=47', 0, 'page', '', 0),
(48, 1, '2019-08-09 17:43:44', '2019-08-09 20:43:44', '', 'Dicas', '', 'inherit', 'closed', 'closed', '', '47-revision-v1', '', '', '2019-08-09 17:43:44', '2019-08-09 20:43:44', '', 47, 'http://localhost/projetos/projeto_lesto_site/47-revision-v1/', 0, 'revision', '', 0),
(49, 1, '2019-08-09 17:45:16', '2019-08-09 20:45:16', '', 'Dicas', '', 'publish', 'closed', 'closed', '', 'dicas', '', '', '2019-08-09 17:45:16', '2019-08-09 20:45:16', '', 0, 'http://localhost/projetos/projeto_lesto_site/?p=49', 6, 'nav_menu_item', '', 0),
(50, 1, '2019-08-12 14:53:27', '2019-08-12 17:53:27', '', 'bannersala', '', 'inherit', 'open', 'closed', '', 'bannersala', '', '', '2019-08-12 14:53:27', '2019-08-12 17:53:27', '', 22, 'http://localhost/projetos/projeto_lesto_site/wp-content/uploads/2019/08/bannersala.png', 0, 'attachment', 'image/png', 0),
(51, 1, '2019-08-12 15:21:57', '2019-08-12 18:21:57', '', 'Sala de atendimento para 03 pessoas', '', 'publish', 'closed', 'closed', '', 'sala-de-atendimento-para-03-pessoas', '', '', '2019-08-12 17:22:58', '2019-08-12 20:22:58', '', 0, 'http://localhost/projetos/projeto_lesto_site/?post_type=sala&#038;p=51', 0, 'sala', '', 0),
(52, 1, '2019-08-12 15:36:10', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-08-12 15:36:10', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/projeto_lesto_site/?post_type=sala&p=52', 0, 'sala', '', 0),
(53, 1, '2019-08-12 15:42:47', '2019-08-12 18:42:47', '', 's10', '', 'inherit', 'open', 'closed', '', 's10', '', '', '2019-08-12 15:42:47', '2019-08-12 18:42:47', '', 0, 'http://localhost/projetos/projeto_lesto_site/wp-content/uploads/2019/08/s10.png', 0, 'attachment', 'image/png', 0),
(54, 1, '2019-08-12 15:42:47', '2019-08-12 18:42:47', '', 's11', '', 'inherit', 'open', 'closed', '', 's11', '', '', '2019-08-12 15:42:47', '2019-08-12 18:42:47', '', 0, 'http://localhost/projetos/projeto_lesto_site/wp-content/uploads/2019/08/s11.png', 0, 'attachment', 'image/png', 0),
(55, 1, '2019-08-12 15:42:48', '2019-08-12 18:42:48', '', 's12', '', 'inherit', 'open', 'closed', '', 's12', '', '', '2019-08-12 15:42:48', '2019-08-12 18:42:48', '', 0, 'http://localhost/projetos/projeto_lesto_site/wp-content/uploads/2019/08/s12.png', 0, 'attachment', 'image/png', 0),
(56, 1, '2019-08-12 15:42:48', '2019-08-12 18:42:48', '', 's1', '', 'inherit', 'open', 'closed', '', 's1', '', '', '2019-08-12 15:42:48', '2019-08-12 18:42:48', '', 0, 'http://localhost/projetos/projeto_lesto_site/wp-content/uploads/2019/08/s1.png', 0, 'attachment', 'image/png', 0),
(57, 1, '2019-08-12 15:42:49', '2019-08-12 18:42:49', '', 's2', '', 'inherit', 'open', 'closed', '', 's2', '', '', '2019-08-12 15:42:49', '2019-08-12 18:42:49', '', 0, 'http://localhost/projetos/projeto_lesto_site/wp-content/uploads/2019/08/s2.png', 0, 'attachment', 'image/png', 0),
(58, 1, '2019-08-12 15:42:50', '2019-08-12 18:42:50', '', 's3', '', 'inherit', 'open', 'closed', '', 's3', '', '', '2019-08-12 15:42:50', '2019-08-12 18:42:50', '', 0, 'http://localhost/projetos/projeto_lesto_site/wp-content/uploads/2019/08/s3.png', 0, 'attachment', 'image/png', 0),
(59, 1, '2019-08-12 15:42:51', '2019-08-12 18:42:51', '', 's4', '', 'inherit', 'open', 'closed', '', 's4', '', '', '2019-08-12 15:42:51', '2019-08-12 18:42:51', '', 0, 'http://localhost/projetos/projeto_lesto_site/wp-content/uploads/2019/08/s4.png', 0, 'attachment', 'image/png', 0),
(60, 1, '2019-08-12 15:42:52', '2019-08-12 18:42:52', '', 's5', '', 'inherit', 'open', 'closed', '', 's5', '', '', '2019-08-12 15:42:52', '2019-08-12 18:42:52', '', 0, 'http://localhost/projetos/projeto_lesto_site/wp-content/uploads/2019/08/s5.png', 0, 'attachment', 'image/png', 0),
(61, 1, '2019-08-12 15:42:53', '2019-08-12 18:42:53', '', 's6', '', 'inherit', 'open', 'closed', '', 's6', '', '', '2019-08-12 15:42:53', '2019-08-12 18:42:53', '', 0, 'http://localhost/projetos/projeto_lesto_site/wp-content/uploads/2019/08/s6.png', 0, 'attachment', 'image/png', 0),
(62, 1, '2019-08-12 15:42:54', '2019-08-12 18:42:54', '', 's7', '', 'inherit', 'open', 'closed', '', 's7', '', '', '2019-08-12 15:42:54', '2019-08-12 18:42:54', '', 0, 'http://localhost/projetos/projeto_lesto_site/wp-content/uploads/2019/08/s7.png', 0, 'attachment', 'image/png', 0),
(63, 1, '2019-08-12 15:42:55', '2019-08-12 18:42:55', '', 's8', '', 'inherit', 'open', 'closed', '', 's8', '', '', '2019-08-12 15:42:55', '2019-08-12 18:42:55', '', 0, 'http://localhost/projetos/projeto_lesto_site/wp-content/uploads/2019/08/s8.png', 0, 'attachment', 'image/png', 0),
(64, 1, '2019-08-12 15:42:56', '2019-08-12 18:42:56', '', 's9', '', 'inherit', 'open', 'closed', '', 's9', '', '', '2019-08-12 15:42:56', '2019-08-12 18:42:56', '', 0, 'http://localhost/projetos/projeto_lesto_site/wp-content/uploads/2019/08/s9.png', 0, 'attachment', 'image/png', 0),
(65, 1, '2019-08-12 15:44:25', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-08-12 15:44:25', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/projeto_lesto_site/?post_type=sala&p=65', 0, 'sala', '', 0),
(66, 1, '2019-08-12 17:23:59', '2019-08-12 20:23:59', '', 'Sala de atendimento para 03 pessoas', '', 'publish', 'closed', 'closed', '', 'sala-de-atendimento-para-03-pessoas-2', '', '', '2019-08-12 17:23:59', '2019-08-12 20:23:59', '', 0, 'http://localhost/projetos/projeto_lesto_site/?post_type=sala&#038;p=66', 0, 'sala', '', 0),
(67, 1, '2019-08-12 17:24:38', '2019-08-12 20:24:38', '', 'Sala de atendimento para 03 pessoas', '', 'publish', 'closed', 'closed', '', 'sala-de-atendimento-para-03-pessoas-3', '', '', '2019-08-12 17:24:38', '2019-08-12 20:24:38', '', 0, 'http://localhost/projetos/projeto_lesto_site/?post_type=sala&#038;p=67', 0, 'sala', '', 0),
(68, 1, '2019-08-12 17:25:17', '2019-08-12 20:25:17', '', 'Sala de reunião para 05 pessoas', '', 'publish', 'closed', 'closed', '', 'sala-de-reuniao-para-05-pessoas', '', '', '2019-08-12 17:25:17', '2019-08-12 20:25:17', '', 0, 'http://localhost/projetos/projeto_lesto_site/?post_type=sala&#038;p=68', 0, 'sala', '', 0),
(69, 1, '2019-08-12 17:25:46', '2019-08-12 20:25:46', '', 'Sala de reunião para 07 pessoas', '', 'publish', 'closed', 'closed', '', 'sala-de-reuniao-para-07-pessoas', '', '', '2019-08-12 17:25:46', '2019-08-12 20:25:46', '', 0, 'http://localhost/projetos/projeto_lesto_site/?post_type=sala&#038;p=69', 0, 'sala', '', 0),
(70, 1, '2019-08-12 17:26:09', '2019-08-12 20:26:09', '', 'Sala de reunião para 09 pessoas', '', 'publish', 'closed', 'closed', '', 'sala-de-reuniao-para-09-pessoas', '', '', '2019-08-12 17:26:09', '2019-08-12 20:26:09', '', 0, 'http://localhost/projetos/projeto_lesto_site/?post_type=sala&#038;p=70', 0, 'sala', '', 0),
(71, 1, '2019-08-12 17:27:00', '2019-08-12 20:27:00', '', 'Sala de atendimento para 03 pessoas', '', 'publish', 'closed', 'closed', '', 'sala-de-atendimento-para-03-pessoas-4', '', '', '2019-08-12 17:27:00', '2019-08-12 20:27:00', '', 0, 'http://localhost/projetos/projeto_lesto_site/?post_type=sala&#038;p=71', 0, 'sala', '', 0),
(72, 1, '2019-08-12 17:27:31', '2019-08-12 20:27:31', '', 'Sala de reunião para 07 pessoas', '', 'publish', 'closed', 'closed', '', 'sala-de-reuniao-para-07-pessoas-2', '', '', '2019-08-12 17:27:31', '2019-08-12 20:27:31', '', 0, 'http://localhost/projetos/projeto_lesto_site/?post_type=sala&#038;p=72', 0, 'sala', '', 0),
(73, 1, '2019-08-12 17:27:51', '2019-08-12 20:27:51', '', 'Sala de reunião para 07 pessoas', '', 'publish', 'closed', 'closed', '', 'sala-de-reuniao-para-07-pessoas-3', '', '', '2019-08-12 17:27:51', '2019-08-12 20:27:51', '', 0, 'http://localhost/projetos/projeto_lesto_site/?post_type=sala&#038;p=73', 0, 'sala', '', 0),
(74, 1, '2019-08-12 17:28:18', '2019-08-12 20:28:18', '', 'Sala de reunião para 05 pessoas', '', 'publish', 'closed', 'closed', '', 'sala-de-reuniao-para-05-pessoas-2', '', '', '2019-08-12 17:28:18', '2019-08-12 20:28:18', '', 0, 'http://localhost/projetos/projeto_lesto_site/?post_type=sala&#038;p=74', 0, 'sala', '', 0),
(75, 1, '2019-08-12 17:28:35', '2019-08-12 20:28:35', '', 'Sala de reunião para 07 pessoas', '', 'publish', 'closed', 'closed', '', 'sala-de-reuniao-para-07-pessoas-4', '', '', '2019-08-12 17:28:35', '2019-08-12 20:28:35', '', 0, 'http://localhost/projetos/projeto_lesto_site/?post_type=sala&#038;p=75', 0, 'sala', '', 0),
(76, 1, '2019-08-12 17:28:54', '2019-08-12 20:28:54', '', 'Sala de reunião para 09 pessoas', '', 'publish', 'closed', 'closed', '', 'sala-de-reuniao-para-09-pessoas-2', '', '', '2019-08-12 17:28:54', '2019-08-12 20:28:54', '', 0, 'http://localhost/projetos/projeto_lesto_site/?post_type=sala&#038;p=76', 0, 'sala', '', 0),
(77, 1, '2019-08-12 17:58:37', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-08-12 17:58:37', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/projeto_lesto_site/?post_type=plano&p=77', 0, 'plano', '', 0),
(78, 1, '2019-08-12 18:06:54', '2019-08-12 21:06:54', '', 'd9', '', 'inherit', 'open', 'closed', '', 'd9', '', '', '2019-08-12 18:06:54', '2019-08-12 21:06:54', '', 0, 'http://localhost/projetos/projeto_lesto_site/wp-content/uploads/2019/08/d9.png', 0, 'attachment', 'image/png', 0),
(79, 1, '2019-08-12 18:06:55', '2019-08-12 21:06:55', '', 'bannerdicas', '', 'inherit', 'open', 'closed', '', 'bannerdicas', '', '', '2019-08-12 18:06:55', '2019-08-12 21:06:55', '', 0, 'http://localhost/projetos/projeto_lesto_site/wp-content/uploads/2019/08/bannerdicas.png', 0, 'attachment', 'image/png', 0),
(80, 1, '2019-08-12 18:06:56', '2019-08-12 21:06:56', '', 'd2', '', 'inherit', 'open', 'closed', '', 'd2', '', '', '2019-08-12 18:06:56', '2019-08-12 21:06:56', '', 0, 'http://localhost/projetos/projeto_lesto_site/wp-content/uploads/2019/08/d2.png', 0, 'attachment', 'image/png', 0),
(81, 1, '2019-08-12 18:06:56', '2019-08-12 21:06:56', '', 'd3', '', 'inherit', 'open', 'closed', '', 'd3', '', '', '2019-08-12 18:06:56', '2019-08-12 21:06:56', '', 0, 'http://localhost/projetos/projeto_lesto_site/wp-content/uploads/2019/08/d3.png', 0, 'attachment', 'image/png', 0),
(82, 1, '2019-08-12 18:06:57', '2019-08-12 21:06:57', '', 'd4', '', 'inherit', 'open', 'closed', '', 'd4', '', '', '2019-08-12 18:06:57', '2019-08-12 21:06:57', '', 0, 'http://localhost/projetos/projeto_lesto_site/wp-content/uploads/2019/08/d4.png', 0, 'attachment', 'image/png', 0),
(83, 1, '2019-08-12 18:06:57', '2019-08-12 21:06:57', '', 'd5', '', 'inherit', 'open', 'closed', '', 'd5', '', '', '2019-08-12 18:06:57', '2019-08-12 21:06:57', '', 0, 'http://localhost/projetos/projeto_lesto_site/wp-content/uploads/2019/08/d5.png', 0, 'attachment', 'image/png', 0),
(84, 1, '2019-08-12 18:06:57', '2019-08-12 21:06:57', '', 'd6', '', 'inherit', 'open', 'closed', '', 'd6', '', '', '2019-08-12 18:06:57', '2019-08-12 21:06:57', '', 0, 'http://localhost/projetos/projeto_lesto_site/wp-content/uploads/2019/08/d6.png', 0, 'attachment', 'image/png', 0),
(85, 1, '2019-08-12 18:06:58', '2019-08-12 21:06:58', '', 'd7', '', 'inherit', 'open', 'closed', '', 'd7', '', '', '2019-08-12 18:06:58', '2019-08-12 21:06:58', '', 0, 'http://localhost/projetos/projeto_lesto_site/wp-content/uploads/2019/08/d7.png', 0, 'attachment', 'image/png', 0),
(86, 1, '2019-08-12 18:06:58', '2019-08-12 21:06:58', '', 'd8', '', 'inherit', 'open', 'closed', '', 'd8', '', '', '2019-08-12 18:06:58', '2019-08-12 21:06:58', '', 0, 'http://localhost/projetos/projeto_lesto_site/wp-content/uploads/2019/08/d8.png', 0, 'attachment', 'image/png', 0),
(87, 1, '2019-08-12 18:08:20', '2019-08-12 21:08:20', '<!-- wp:paragraph -->\n<p>Boas-vindas ao WordPress. Esse é o seu primeiro post. Edite-o ou exclua-o, e então comece a escrever!</p>\n<!-- /wp:paragraph -->', 'Olá, mundo!', '', 'inherit', 'closed', 'closed', '', '1-revision-v1', '', '', '2019-08-12 18:08:20', '2019-08-12 21:08:20', '', 1, 'http://localhost/projetos/projeto_lesto_site/1-revision-v1/', 0, 'revision', '', 0),
(88, 1, '2019-08-12 18:09:11', '2019-08-12 21:09:11', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'RH Lorem Ipsum', '', 'publish', 'open', 'open', '', 'rh-lorem-ipsum', '', '', '2019-08-12 18:21:41', '2019-08-12 21:21:41', '', 0, 'http://localhost/projetos/projeto_lesto_site/?p=88', 0, 'post', '', 0),
(89, 1, '2019-08-12 18:09:02', '2019-08-12 21:09:02', '', 'd1', '', 'inherit', 'open', 'closed', '', 'd1', '', '', '2019-08-12 18:09:02', '2019-08-12 21:09:02', '', 88, 'http://localhost/projetos/projeto_lesto_site/wp-content/uploads/2019/08/d1.png', 0, 'attachment', 'image/png', 0),
(94, 1, '2019-08-12 18:21:41', '2019-08-12 21:21:41', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'RH Lorem Ipsum', '', 'inherit', 'closed', 'closed', '', '88-revision-v1', '', '', '2019-08-12 18:21:41', '2019-08-12 21:21:41', '', 88, 'http://localhost/projetos/projeto_lesto_site/88-revision-v1/', 0, 'revision', '', 0),
(90, 1, '2019-08-12 18:09:11', '2019-08-12 21:09:11', '<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p></blockquote>\n<!-- /wp:quote -->', 'RH Lorem Ipsum', '', 'inherit', 'closed', 'closed', '', '88-revision-v1', '', '', '2019-08-12 18:09:11', '2019-08-12 21:09:11', '', 88, 'http://localhost/projetos/projeto_lesto_site/88-revision-v1/', 0, 'revision', '', 0),
(91, 1, '2019-08-12 18:17:36', '2019-08-12 21:17:36', '<!-- wp:paragraph -->\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\n<!-- /wp:paragraph -->', 'RH Lorem Ipsum', '', 'inherit', 'closed', 'closed', '', '88-revision-v1', '', '', '2019-08-12 18:17:36', '2019-08-12 21:17:36', '', 88, 'http://localhost/projetos/projeto_lesto_site/88-revision-v1/', 0, 'revision', '', 0),
(92, 1, '2019-08-12 18:19:41', '2019-08-12 21:19:41', '<!-- wp:paragraph -->\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\n<!-- /wp:paragraph -->', 'RH Lorem Ipsum', '', 'inherit', 'closed', 'closed', '', '88-autosave-v1', '', '', '2019-08-12 18:19:41', '2019-08-12 21:19:41', '', 88, 'http://localhost/projetos/projeto_lesto_site/88-autosave-v1/', 0, 'revision', '', 0),
(93, 1, '2019-08-12 18:20:10', '2019-08-12 21:20:10', '<!-- wp:paragraph -->\n<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>\n<!-- /wp:paragraph -->', 'Bloco reutilizável sem título', '', 'publish', 'closed', 'closed', '', 'bloco-reutilizavel-sem-titulo', '', '', '2019-08-12 18:20:10', '2019-08-12 21:20:10', '', 0, 'http://localhost/projetos/projeto_lesto_site/bloco-reutilizavel-sem-titulo/', 0, 'wp_block', '', 0),
(95, 1, '2019-08-13 14:06:20', '2019-08-13 17:06:20', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'RH Lorem Ipsum', '', 'publish', 'open', 'open', '', 'rh-lorem-ipsum-2', '', '', '2019-08-13 14:06:20', '2019-08-13 17:06:20', '', 0, 'http://localhost/projetos/projeto_lesto_site/?p=95', 1, 'post', '', 0),
(96, 1, '2019-08-13 14:06:20', '2019-08-13 17:06:20', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'RH Lorem Ipsum', '', 'inherit', 'closed', 'closed', '', '95-revision-v1', '', '', '2019-08-13 14:06:20', '2019-08-13 17:06:20', '', 95, 'http://localhost/projetos/projeto_lesto_site/95-revision-v1/', 0, 'revision', '', 0),
(97, 1, '2019-08-13 14:06:52', '2019-08-13 17:06:52', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Baba, amet sit', '', 'publish', 'open', 'open', '', 'baba-amet-sit', '', '', '2019-08-13 14:06:52', '2019-08-13 17:06:52', '', 0, 'http://localhost/projetos/projeto_lesto_site/?p=97', 2, 'post', '', 0),
(98, 1, '2019-08-13 14:06:52', '2019-08-13 17:06:52', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'Baba, amet sit', '', 'inherit', 'closed', 'closed', '', '97-revision-v1', '', '', '2019-08-13 14:06:52', '2019-08-13 17:06:52', '', 97, 'http://localhost/projetos/projeto_lesto_site/97-revision-v1/', 0, 'revision', '', 0);
INSERT INTO `le_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(99, 1, '2019-08-13 14:07:32', '2019-08-13 17:07:32', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'RH Lorem Ipsum', '', 'publish', 'open', 'open', '', 'rh-lorem-ipsum-3', '', '', '2019-08-13 14:07:32', '2019-08-13 17:07:32', '', 0, 'http://localhost/projetos/projeto_lesto_site/?p=99', 3, 'post', '', 0),
(100, 1, '2019-08-13 14:07:32', '2019-08-13 17:07:32', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'RH Lorem Ipsum', '', 'inherit', 'closed', 'closed', '', '99-revision-v1', '', '', '2019-08-13 14:07:32', '2019-08-13 17:07:32', '', 99, 'http://localhost/projetos/projeto_lesto_site/99-revision-v1/', 0, 'revision', '', 0),
(101, 1, '2019-08-13 14:08:12', '2019-08-13 17:08:12', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'RH Lorem Ipsum', '', 'publish', 'open', 'open', '', 'rh-lorem-ipsum-4', '', '', '2019-08-13 14:10:41', '2019-08-13 17:10:41', '', 0, 'http://localhost/projetos/projeto_lesto_site/?p=101', 4, 'post', '', 0),
(102, 1, '2019-08-13 14:08:12', '2019-08-13 17:08:12', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'RH Lorem Ipsum', '', 'inherit', 'closed', 'closed', '', '101-revision-v1', '', '', '2019-08-13 14:08:12', '2019-08-13 17:08:12', '', 101, 'http://localhost/projetos/projeto_lesto_site/101-revision-v1/', 0, 'revision', '', 0),
(103, 1, '2019-08-13 14:08:52', '2019-08-13 17:08:52', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'RH Lorem Ipsum', '', 'publish', 'open', 'open', '', 'rh-lorem-ipsum-5', '', '', '2019-08-13 14:08:52', '2019-08-13 17:08:52', '', 0, 'http://localhost/projetos/projeto_lesto_site/?p=103', 5, 'post', '', 0),
(104, 1, '2019-08-13 14:08:52', '2019-08-13 17:08:52', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'RH Lorem Ipsum', '', 'inherit', 'closed', 'closed', '', '103-revision-v1', '', '', '2019-08-13 14:08:52', '2019-08-13 17:08:52', '', 103, 'http://localhost/projetos/projeto_lesto_site/103-revision-v1/', 0, 'revision', '', 0),
(105, 1, '2019-08-13 14:08:59', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-08-13 14:08:59', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/projeto_lesto_site/?p=105', 0, 'post', '', 0),
(106, 1, '2019-08-13 14:09:16', '2019-08-13 17:09:16', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'RH Lorem Ipsum', '', 'publish', 'open', 'open', '', 'rh-lorem-ipsum-6', '', '', '2019-08-13 14:09:16', '2019-08-13 17:09:16', '', 0, 'http://localhost/projetos/projeto_lesto_site/?p=106', 6, 'post', '', 0),
(107, 1, '2019-08-13 14:09:16', '2019-08-13 17:09:16', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'RH Lorem Ipsum', '', 'inherit', 'closed', 'closed', '', '106-revision-v1', '', '', '2019-08-13 14:09:16', '2019-08-13 17:09:16', '', 106, 'http://localhost/projetos/projeto_lesto_site/106-revision-v1/', 0, 'revision', '', 0),
(108, 1, '2019-08-13 14:09:37', '2019-08-13 17:09:37', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'RH Lorem Ipsum', '', 'publish', 'open', 'open', '', 'rh-lorem-ipsum-7', '', '', '2019-08-13 14:09:37', '2019-08-13 17:09:37', '', 0, 'http://localhost/projetos/projeto_lesto_site/?p=108', 7, 'post', '', 0),
(109, 1, '2019-08-13 14:09:37', '2019-08-13 17:09:37', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'RH Lorem Ipsum', '', 'inherit', 'closed', 'closed', '', '108-revision-v1', '', '', '2019-08-13 14:09:37', '2019-08-13 17:09:37', '', 108, 'http://localhost/projetos/projeto_lesto_site/108-revision-v1/', 0, 'revision', '', 0),
(110, 1, '2019-08-13 14:09:58', '2019-08-13 17:09:58', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'RH Lorem Ipsum', '', 'publish', 'open', 'open', '', 'rh-lorem-ipsum-8', '', '', '2019-08-13 14:09:58', '2019-08-13 17:09:58', '', 0, 'http://localhost/projetos/projeto_lesto_site/?p=110', 8, 'post', '', 0),
(111, 1, '2019-08-13 14:09:58', '2019-08-13 17:09:58', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'RH Lorem Ipsum', '', 'inherit', 'closed', 'closed', '', '110-revision-v1', '', '', '2019-08-13 14:09:58', '2019-08-13 17:09:58', '', 110, 'http://localhost/projetos/projeto_lesto_site/110-revision-v1/', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `le_termmeta`
--

DROP TABLE IF EXISTS `le_termmeta`;
CREATE TABLE IF NOT EXISTS `le_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `le_terms`
--

DROP TABLE IF EXISTS `le_terms`;
CREATE TABLE IF NOT EXISTS `le_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `le_terms`
--

INSERT INTO `le_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Sem categoria', 'sem-categoria', 0),
(2, 'Menu principal', 'menu-principal', 0),
(3, 'Menu footer', 'menu-footer', 0),
(4, 'Trajano Reis, 472', 'trajano-reis', 0),
(5, 'Centro Cívico Neo Business', 'centro-civico', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `le_term_relationships`
--

DROP TABLE IF EXISTS `le_term_relationships`;
CREATE TABLE IF NOT EXISTS `le_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `le_term_relationships`
--

INSERT INTO `le_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(41, 2, 0),
(38, 2, 0),
(39, 2, 0),
(37, 2, 0),
(40, 2, 0),
(42, 3, 0),
(43, 3, 0),
(44, 3, 0),
(45, 3, 0),
(46, 3, 0),
(49, 3, 0),
(66, 4, 0),
(51, 4, 0),
(67, 4, 0),
(68, 4, 0),
(69, 4, 0),
(70, 4, 0),
(71, 5, 0),
(72, 5, 0),
(73, 5, 0),
(74, 5, 0),
(75, 5, 0),
(76, 5, 0),
(88, 1, 0),
(95, 1, 0),
(97, 1, 0),
(99, 1, 0),
(101, 1, 0),
(103, 1, 0),
(106, 1, 0),
(108, 1, 0),
(110, 1, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `le_term_taxonomy`
--

DROP TABLE IF EXISTS `le_term_taxonomy`;
CREATE TABLE IF NOT EXISTS `le_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `le_term_taxonomy`
--

INSERT INTO `le_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 9),
(2, 2, 'nav_menu', '', 0, 5),
(3, 3, 'nav_menu', '', 0, 6),
(4, 4, 'categoriaSalas', 'Obs. (1) - Em todas as salas são oferecidos como cortesia, secretária para recepcionar o cliente até a sala, água, café e internet wireless.  (2) - Estacionamento próprio de cortesia, para todos, durante o período da reunião, limitado a 15 vagas rotativas. (3) - Exclusivamente nas salas de reunião, são oferecidos como cortesia, tela de LED, água e café expresso diretamente na sala. (4) - Todas as salas possuem AR Condicionado, exceto a primeira sala de atendimento e a sala de reunião para 05 pessoas.', 0, 6),
(5, 5, 'categoriaSalas', 'Obs. (1) - Em todas as salas são oferecidos como cortesia, secretária para recepcionar o cliente até a sala, água, café, internet wireless e exclusivamente nas salas de reunião, tela de LED.  (2) - Todas as salas possuem AR Condicionado.', 0, 6);

-- --------------------------------------------------------

--
-- Estrutura da tabela `le_usermeta`
--

DROP TABLE IF EXISTS `le_usermeta`;
CREATE TABLE IF NOT EXISTS `le_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `le_usermeta`
--

INSERT INTO `le_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'lestoescritorios'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'le_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'le_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '0'),
(16, 1, 'session_tokens', 'a:1:{s:64:\"a0c7c84fe3e6e26215a787314fca745d1c866c2f11caa12c253fdb0960eb8f38\";a:4:{s:10:\"expiration\";i:1565805013;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36\";s:5:\"login\";i:1565632213;}}'),
(17, 1, 'le_dashboard_quick_press_last_post_id', '4'),
(18, 1, 'le_user-settings', 'mfold=o&editor=tinymce&libraryContent=browse'),
(19, 1, 'le_user-settings-time', '1565383318'),
(20, 1, 'closedpostboxes_dashboard', 'a:0:{}'),
(21, 1, 'metaboxhidden_dashboard', 'a:4:{i:0;s:19:\"dashboard_right_now\";i:1;s:18:\"dashboard_activity\";i:2;s:21:\"dashboard_quick_press\";i:3;s:17:\"dashboard_primary\";}'),
(22, 1, 'le_r_tru_u_x', 'a:2:{s:2:\"id\";i:0;s:7:\"expires\";i:1565385985;}'),
(23, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(24, 1, 'metaboxhidden_nav-menus', 'a:7:{i:0;s:22:\"add-post-type-destaque\";i:1;s:19:\"add-post-type-plano\";i:2;s:18:\"add-post-type-sala\";i:3;s:21:\"add-post-type-unidade\";i:4;s:12:\"add-post_tag\";i:5;s:21:\"add-categoriaDestaque\";i:6;s:18:\"add-categoriaSalas\";}'),
(25, 1, 'nav_menu_recently_edited', '3');

-- --------------------------------------------------------

--
-- Estrutura da tabela `le_users`
--

DROP TABLE IF EXISTS `le_users`;
CREATE TABLE IF NOT EXISTS `le_users` (
  `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `le_users`
--

INSERT INTO `le_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'lestoescritorios', '$P$BxKMHbtu.I5aynPRmk0wqrc/HeRwVs/', 'lestoescritorios', 'devhcdesenvolvimentos@gmail.com', '', '2019-08-08 19:19:51', '', 0, 'lestoescritorios');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
