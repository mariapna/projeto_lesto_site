<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'projetos_lesto_site' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Vlv;2@0Sf$t&S!`US Tu+/PH$tK+Z[);&Fh1}0WNvC7vJCq*sz_b]c_RPop~Aan0' );
define( 'SECURE_AUTH_KEY',  'bu[gW$OaaCN6,d>tOB@`BtL]l;I0?QZv:lENYF!DC~lJp*mSLa6]%H4XJQLk$2Pd' );
define( 'LOGGED_IN_KEY',    '*K/&O,>YLUC>;r.OAQrEO8txm! 4-p?+xGL(SEzU-1R!]0LI%a_ MW5PFI^7d( b' );
define( 'NONCE_KEY',        'u+pnzspr+$s<:g7aj$7TVUBbk3nM5L3 aHqQ:MRk.2? T7j0o2:5p[y5x^, ?/!9' );
define( 'AUTH_SALT',        'BuZydD/v^LuX3@l&8eNgV54mXXs9RKOT Jb?*Phe$g>ej.6GVEh%T|4?6_?W03 =' );
define( 'SECURE_AUTH_SALT', 'Yb,>QT-*Wch;wnr6?,f;~k00di49 9^V(r50ZrQr84o]1f{(D0a:r0kj4N%(BWgA' );
define( 'LOGGED_IN_SALT',   '?#Y3E1t9GK&6]z>3R]l`X 6J4|^$q.V|Tmx&bv68}mF7+ruz,8cb}R_ihtgAE$]*' );
define( 'NONCE_SALT',       'WS!D:*R<Ndr96l?E70Nmf9~62K.t2^CJ0H[dw8VzvCHmw%ZXe=;E^wZ:3^a]QuZK' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'le_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
