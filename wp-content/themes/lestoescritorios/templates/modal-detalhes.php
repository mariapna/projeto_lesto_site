<!-- MODAL DETALHES -->
<div class="sala-detalhes">	

	<!-- CARROSSEL DE GALERIA -->
	<section class="carrosselDetalhes">
		<span class="fecharModal">X</span>
		<button class="carrosselDetalhesEsquerda"></button>
		<button class="carrosselDetalhesDireita"></button>
		<h6 class="hidden">Carrossel Destaque</h6>
		<div id="carrosselDetalhes" class="owl-Carousel">
			
			<!-- ITEM -->
			<figure class="item">
				<a href="">
					<img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/bannerTopoHome.jpg" alt="Nome destaque">
				</a>
			</figure>
			
			<!-- ITEM -->
			<figure class="item">
				<a href="">
					<img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/bannerTopoHome.jpg" alt="Nome destaque">
				</a>
			</figure>

		</div>
	</section>
</div>