    <!-- MODAL CONTRATE -->
    <section class="lesto-fale-conosco contrate">
    	<div class="container">
    		<form action="" method="post">
    			<span class="fecharModal"><i class="fas fa-times"></i></span>

    			<div class="row">

    				<div class="form-titulo">
    					<span> Contratar Serviço </span>
    				</div>
    				<div class="form-subtitulo">
    					<span> Se interessou? Receba os valores no seu e-mail</span>
    					<p>Preencha o formulário para saber mais sobre os planos de coworking! Em breve, entramos em contato</p>
    				</div>
    				<div class="col-md-12">
    					<div class="form-info">
    						<div class="info">
    							<label for="nome"> Seu Nome</label>
    							<input type="text" id="nome" Placeholder="Nome" />
    						</div>
    						<div  class="info">
    							<label for="email">Seu e-email</label>
    							<input type="text" id="email" Placeholder="E-mail" />
    						</div>
    						<div  class="info full">
    							<label for="assunto">Assunto</label>
    							<input type="text" id="assunto" Placeholder="Assunto" />
    						</div>
    						<div class="info full textarea">
    							<label for="mensagem" >Mensagem</label>
    							<textarea Placeholder="Mensagem"> </textarea> 
    						</div>
    						<div class="btn-enviar">
    							<input type="submit"> </input>
    						</div>
    					</div>
    				</div>
    				
    			</div>
    		</form>					
    	</div>	
    </div>			
</section>