<?php
/**
* The template for displaying archive pages.
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package lestoescritorios
*/

get_header();
?>

<!-- PG SALAS  -->
<div class="pg salas">

	<!-- BANNER TOPO -->
	<section class="bannerPagina" style="background: url(<?php echo get_template_directory_uri(); ?>/img/bannersala.png);">
		<!-- <h2 class="nomePagina">Salas</h2> -->
	</section>

	<!-- SESSÃO SALA TRAJANO  -->
	<section class="salas-lesto">
		<h6 class="hidden">Sessão salas lesto</h6>
		<div class="container">                	
			<?php

			// DEFININDO TIPO DE TAXONOMIA
			$categoriasSalas = 'categoriaSalas';

			// PEGANDO TODAS AS CATEGORIAS DA TAXONOMIA
			$listaCategoriasSalas = get_terms( $categoriasSalas, array('orderby' => 'count','hide_empty' => 0,'parent'	 => 0));
			// var_dump($listaCategoriasSalas);
			// LISTANDO CADA CATEGORIA DA TAXONOMIA
			foreach($listaCategoriasSalas as $categoria):
				$slugCategoria = $categoria->slug;
				$nomeCategoria = $categoria->name;
				$descricaoCategoria = $categoria->description;

				// LISTA DE SALAS
				$listaSalas = new WP_Query( 
					array(
						'post_type' => 'sala',
						'posts_per_page' => -1,
						'orderby' => 'id',

					 	// FILTRO DE SALAS POR CATEGORIAS, ATRAVÉS DO SLUG
						'tax_query' => array(
							array(
								'taxonomy' => $categoriasSalas,
								'field'    => 'slug',
								'terms'    => $slugCategoria,
							)
						)
					)
				);

			?>

			<div class="conteudo"> 						
				<!-- TÍTULO -->
				<div class="titulo">
					<span class="">Salas da unidade <?php echo $nomeCategoria; ?></span>
				</div>

				<div class="unidades">

					<?php
					while($listaSalas->have_posts()): $listaSalas->the_post();
						$imagemSala = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0];

					?>
					<!--DIV SALA -->
					<div class="lesto-sala">
						<figure>
							<img src="<?php echo $imagemSala; ?>">
							<figcaption class="hidden">Imagem da sala</figcaption>	
						</figure>
						<div class="info">
							<h4><?php echo get_the_title(); ?></h4>
							<div class="clientes">

								<div class="ambiente-lesto">
									<span> Clientes lesto</span>
									<p><?php echo rwmb_meta('LestoEscritorios_preco_clientes_lesto'); ?></p>
								</div>
								<div class="ambiente-lesto right">
									<span> Clientes Externos</span>
									<p><?php echo rwmb_meta('LestoEscritorios_preco_clientes_externos'); ?></p>
								</div>
							</div>
							<div class="mais-sobre">
								<ul>
									<li class="abrirModalDetalhes"><a href=""></a> Detalhes</li>
									<li class="abrirModalContrate rigth"><a href=""></a> Contrate</li>
								</ul>
							</div>
						</div>
					</div>

					<?php endwhile; ?>
				</div>

				<div class="observacao">
					<p><?php echo $descricaoCategoria; ?></p>
				</div>	
			</div> 

			<?php endforeach; ?>
		</div>

	</section>	
	<!-- SESSÃO FALE CONOSCO HOME -->
	<section class="lesto-fale-conosco">
		<div class="container">
			<form action="" method="post">
				<div class="row">
					<div class="form-titulo">
						<span> Fale conosco </span>
					</div>
                    <!-- <div class="form-subtitulo">
                    	<span> Se interessou? Receba os valores no seu e-mail</span>
                    	<p>Preencha o formulário para saber mais sobre os planos de coworking! Em breve, entramos em contato</p>
                    </div> -->								
                    <div class="col-md-12">
                    	<div class="form-info">
                    		<div class="info">
                    			<label for="nome"> Seu Nome</label>
                    			<input type="text" id="nome" Placeholder="Nome" />
                    		</div>
                    		<div  class="info">
                    			<label for="email">Seu e-mail</label>
                    			<input type="text" id="email" Placeholder="Seu e-mail" />
                    		</div>
                    		<div  class="info full">
                    			<label for="assunto">Assunto</label>
                    			<input type="text" id="assunto" Placeholder="Assunto" />
                    		</div>
                    		<div class="info full textarea">
                    			<label for="mensagem" >Mensagem</label>
                    			<textarea Placeholder="Mensagem"> </textarea> 
                    		</div>
                    		<div class="btn-enviar">
                    			<input type="submit"> </input>
                    		</div>
                    	</div>
                    </div>
					<!-- <div class="col-md-6">
						<div class="area-contato">
							<div class="contato-titulo">
							    <span> Contato: </span>
						    </div>
						    <div class="lesto-outros-contatos">
						    	<ul>
						    		<li>
						    			<div class="icone-contato">
						    				<i class="fas fa-envelope"></i>
						    			</div>
										<span>contato@lesto.com.br</span>
						    		</li>
						    		<li>
						    			<div class="icone-contato meio">
											<i class="fas fa-phone"></i>		
						    			</div>
										<span>(41)3122-2000</span>
						    		</li>
						    		<li>
						    			<div class="icone-contato">
						    				<i class="fab fa-whatsapp"></i>
						    			</div>
										<span>(41)3122-2000</span>
						    		</li>
						    	</ul>							    	
						    </div>
						</div>								
					</div> -->	
				</div>
			</form>					
		</div>			
	</section>
</div>

<?php include(TEMPLATEPATH . "/templates/modal-detalhes.php"); ?>
<?php include(TEMPLATEPATH . "/templates/modal-contrate.php"); ?>

<?php get_footer();