<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package lestoescritorios
 */

global $configuracao;

?>

		<!-- RODAPÉ -->
		<footer class="lesto-rodape">
			<div class="container">	
				<div class="conteudo">
				<ul>
					<li>
						<div class="areaLogo">
							<img src="<?php echo $configuracao['opt_logo']['url'] ?>" alt="">
					    </div>
					</li>
					<li>
						<div class="info-footer informacoes">
							<h5 class="titulo">Informações</h5>
							<div class="contato-footer">
								<span class="item-footer">contato@lest.com.br</span>
								<span class="item-footer">(41) 3122-2000</span>
								<span class="item-footer">(41) 3122-2000</span>
								<span class="item-footer">Av. Candido de Abreu, 470 14, andar Sala 1407, Curitiba</span>
								<span class="item-footer">(41) 3122-2000</span>
							</div>
						</div>
					</li>
					<li>
						<div class="info-footer acesso-rapido">
							<h5 class="titulo">Acesso Rápido</h5>
							<nav class="nav-footer">
								<!-- <a href="#" class="item-nav-footer">Home</a>
								<a href="#" class="item-nav-footer">Planos </a>
								<a href="#" class="item-nav-footer">Sala</a>
								<a href="#" class="item-nav-footer">Localização</a>
								<a href="#" class="item-nav-footer">Contato</a>
								<a href="#" class="item-nav-footer">Dicas</a> -->
								<?php 
								$menu = array(
									'theme_location'  => '',
									'menu'            => 'Menu footer',
									'container'       => false,
									'container_class' => '',
									'container_id'    => '',
									'menu_class'      => 'menu',
									'menu_id'         => '',
									'echo'            => true,
									'fallback_cb'     => 'wp_page_menu',
									'before'          => '',
									'after'           => '',
									'link_before'     => '',
									'link_after'      => '',
									'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
									'depth'           => 2,
									'walker'          => ''
								);
								wp_nav_menu( $menu );
								?>
							</nav>
						</div>
					</li>
					<li>
						<div class="info-footer instagram">
							<h5 class="titulo"> Instagram</h5>
							<div class="foto-insta">
								<img src="<?php echo get_template_directory_uri(); ?>/img/insta-lesto.png" alt="">
							</div>
						</div>
					</li>
					<li>
						<div class="info-footer facebook">
							<h5 class="titulo">Facebook</h5>
							<div class="facebook">
								<div class="fb-page" data-href="https://www.facebook.com/lestoescritorios" data-tabs="timeline" data-width="230" data-height="200" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/lestoescritorios" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/lestoescritorios">Lesto Escritórios</a></blockquote></div>
							</div>
						</div>
					</li>
				</ul>
				</div>
				<div class="copyright">
					<div class="info">
						<p><?php echo $confifuracao['opt_copyright'] ?></p>
					</div>
					<div class="redes-sociais">
						<ul class="rede-social">
							<li class="li-rede-social"><a href="#"><i class="fab fa-twitter"></i></a></li>
							<li class="li-rede-social"><a href="#"><i class="fab fa-instagram"></i></a></li>
							<li class="li-rede-social "><a href="#"><i class="fab fa-facebook-f"></i></a></li>
							<li class="li-rede-social "><a href="#"><i class="fab fa-whatsapp"></i></a></li>
							<li class="li-rede-social"><a href="#"><i class="fab fa-linkedin-in"></i></a></li>						
						</ul>
					</div>
				</div>
		    </div>
		</footer>	
		<?php wp_footer(); ?>	
	</body>
</html>