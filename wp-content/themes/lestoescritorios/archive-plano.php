<?php
/**
* The template for displaying archive pages.
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package lestoescritorios
*/

get_header();
?>

<!-- PG PLANOS -->
<div class="pg planos">

	<!-- BANNER TOPO -->
	<section class="bannerPagina" style="background: url(<?php echo get_template_directory_uri(); ?>/img/bannerTopoHome.jpg);">
		<!-- <h2 class="nomePagina">Planos</h2> -->
	</section>

	<!-- SESSÃO PLANOS  -->
	<section class="planos-detalhados">
		<h6 class="hidden">Sessão planos lesto</h6>

		<!-- TÍTULO -->
		<div class="titulo">
			<span class=""><?php echo get_the_title(); ?></span>
		</div>

		<div class="plano"> 

			<div class="row">

				<div class="col-sm-6">
					<figure>
						<img src="<?php echo get_template_directory_uri(); ?>/img/escritorio.png">
						<figcaption class="hidden">Imagem Escritório</figcaption>	
					</figure>

				</div>

				<div class="col-sm-6">

					<!-- INFO DO PLANO -->
					<div class="info-plano">
						<article>
							<h3> Endereço comercial unidade trajano Reis </h3>
							<strong class="valor">R$99/mês</strong> 
							<ul>
								<li> <strong> Estacionamento proprio de cortesia </strong> </li>
								<li> <strong> Desconto de 30% na utilizacao das salas </strong> </li>
								<li> <strong> Desconto de 30% na utilizacao das salas </strong> </li>
								<li> <strong> Desconto de 30% na utilizacao das salas </strong> </li>
								<li> <strong> Desconto de 30% na utilizacao das salas </strong> </li>
							</ul> 
							<p>*Obs. O estacionamento de cortesia está limitado a 15 vagas rotativas</p>							
									
						</article>
					</div>
				</div>
			</div>
		</div>

		<div class="plano"> 

			<div class="row">

				<div class="col-sm-6">

					<!-- INFO DO PLANO -->
					<div class="info-plano left">
						<article>
							<h3> Endereço comercial unidade trajano Reis </h3>
							<strong class="valor">R$99/mês</strong> 
							<ul>
								<li> <strong> Estacionamento proprio de cortesia </strong> </li>
								<li> <strong> Desconto de 30% na utilizacao das salas </strong> </li>
								<li> <strong> Desconto de 30% na utilizacao das salas </strong> </li>
								<li> <strong> Desconto de 30% na utilizacao das salas </strong> </li>
								<li> <strong> Desconto de 30% na utilizacao das salas </strong> </li>
							</ul> 
							<p>*Obs. O estacionamento de cortesia está limitado a 15 vagas rotativas</p>							
									
						</article>
					</div>
				</div>
				<div class="col-sm-6">
					<figure>
						<img src="<?php echo get_template_directory_uri(); ?>/img/escritorio.png">
						<figcaption class="hidden">Imagem Escritório</figcaption>	
					</figure>

				</div>
			</div>
		</div>

		<div class="plano"> 

			<div class="row">

				<div class="col-sm-6">
					<figure>
						<img src="<?php echo get_template_directory_uri(); ?>/img/escritorio.png">
						<figcaption class="hidden">Imagem Escritório</figcaption>	
					</figure>

				</div>

				<div class="col-sm-6">

					<!-- INFO DO PLANO -->
					<div class="info-plano">
						<article>
							<h3> Endereço comercial unidade trajano Reis </h3>
							<strong class="valor">R$99/mês</strong> 
							<ul>
								<li> <strong> Estacionamento proprio de cortesia </strong> </li>
								<li> <strong> Desconto de 30% na utilizacao das salas </strong> </li>
								<li> <strong> Desconto de 30% na utilizacao das salas </strong> </li>
								<li> <strong> Desconto de 30% na utilizacao das salas </strong> </li>
								<li> <strong> Desconto de 30% na utilizacao das salas </strong> </li>
							</ul> 
							<p>*Obs. O estacionamento de cortesia está limitado a 15 vagas rotativas</p>							
									
						</article>
					</div>
				</div>
			</div>
		</div>

		<div class="plano"> 

			<div class="row">

				<div class="col-sm-6">

					<!-- INFO DO PLANO -->
					<div class="info-plano left">
						<article>
							<h3> Endereço comercial unidade trajano Reis </h3>
							<strong class="valor">R$99/mês</strong> 
							<ul>
								<li> <strong> Estacionamento proprio de cortesia </strong> </li>
								<li> <strong> Desconto de 30% na utilizacao das salas </strong> </li>
								<li> <strong> Desconto de 30% na utilizacao das salas </strong> </li>
								<li> <strong> Desconto de 30% na utilizacao das salas </strong> </li>
								<li> <strong> Desconto de 30% na utilizacao das salas </strong> </li>
							</ul> 
							<p>*Obs. O estacionamento de cortesia está limitado a 15 vagas rotativas</p>							
									
						</article>
					</div>
				</div>
				<div class="col-sm-6">
					<figure>
						<img src="<?php echo get_template_directory_uri(); ?>/img/escritorio.png">
						<figcaption class="hidden">Imagem Escritório</figcaption>	
					</figure>

				</div>
			</div>
		</div>

		<div class="plano"> 

			<div class="row">

				<div class="col-sm-6">
					<figure>
						<img src="<?php echo get_template_directory_uri(); ?>/img/escritorio.png">
						<figcaption class="hidden">Imagem Escritório</figcaption>	
					</figure>

				</div>

				<div class="col-sm-6">

					<!-- INFO DO PLANO -->
					<div class="info-plano ">
						<article>
							<h3> Endereço comercial unidade trajano Reis </h3>
							<strong class="valor">R$99/mês</strong> 
							<ul>
								<li> <strong> Estacionamento proprio de cortesia </strong> </li>
								<li> <strong> Desconto de 30% na utilizacao das salas </strong> </li>
								<li> <strong> Desconto de 30% na utilizacao das salas </strong> </li>
								<li> <strong> Desconto de 30% na utilizacao das salas </strong> </li>
								<li> <strong> Desconto de 30% na utilizacao das salas </strong> </li>
							</ul> 
							<p>*Obs. O estacionamento de cortesia está limitado a 15 vagas rotativas</p>							
									
						</article>
					</div>
				</div>
			</div>
		</div>

	</section>

	<!-- SESSÃO FALE CONOSCO HOME -->
	<section class="lesto-fale-conosco">
		<div class="container">
			<form action="" method="post">
				<div class="row">
					<div class="form-titulo">
						<span> Fale conosco </span>
					</div>
					<div class="form-subtitulo">
						<span> Se interessou? Receba os valores no seu e-mail</span>
						<p>Preencha o formulário para saber mais sobre os planos de coworking! Em breve, entramos em contato</p>
					</div>
					<div class="col-md-6">
						<div class="form-info">
							<!-- <div class="info">
								<label for="nome"> Seu Nome</label>
								<input type="text" id="nome" Placeholder="Nome" />
							</div>
							<div  class="info">
								<label for="email">Seu e-email</label>
								<input type="text" id="email" Placeholder="E-mail" />
							</div>
							<div  class="info full">
								<label for="assunto">Assunto</label>
								<input type="text" id="assunto" Placeholder="Assunto" />
							</div>
							<div class="info full textarea">
								<label for="mensagem" >Mensagem</label>
								<textarea Placeholder="Mensagem"> </textarea> 
							</div>
							<div class="btn-enviar">
								<input type="submit"> </input>
							</div> -->
							<?php echo do_shortcode('[contact-form-7 id="5" title="Fomulário de contato"]'); ?>
						</div>
					</div>
					<div class="col-md-6">
						<div class="area-contato">
							<div class="contato-titulo">
								<span> Contato: </span>
							</div>
							<div class="lesto-outros-contatos">
								<ul>
									<li>
										<div class="icone-contato">
											<i class="fas fa-envelope"></i>
										</div>
										<span>contato@lesto.com.br</span>
									</li>
									<li>
										<div class="icone-contato meio">
											<i class="fas fa-phone"></i>		
										</div>
										<span>(41)3122-2000</span>
									</li>
									<li>
										<div class="icone-contato">
											<i class="fab fa-whatsapp"></i>
										</div>
										<span>(41)3122-2000</span>
									</li>
								</ul>							    	
							</div>
						</div>								
					</div>	
				</div>
			</form>					
		</div>	
	</div>			
   </section>
</div>

<?php get_footer();