a<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package lestoescritorios
 */

get_header();
?>

<!-- PG POST -->
<div class="pg post">


	<!-- BANNER TOPO -->
	<section class="bannerPagina" style="background: url(<?php echo get_template_directory_uri(); ?>/img/bannerTopoHome.jpg);">
		<!-- <h2 class="nomePagina">O Desafio Olímpico e a Empresa</h2> -->
	</section>

	<section class="lesto-post"> 
		<div class="container">
			<div class="titulo">
				<span> O Desafio Olímpico e a Empresa </span>
			</div>
			<div class="texto">
				<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.Excepteur sint occaExcepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.ecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
				<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip eExcepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.x ea commodo consequat.</p>


			</div>
		</div>
	</section>

	<section class="post-relacionados">
		<div class="container">
			<div class="conteudo">
				<div class="titulo">
					<span>Fique por dentro de algumas dicas que podem favorecer o seu negocio</span>
				</div>

				<ul>
					<li>
						<h4>Rh Lorem Ipsum</h4>
						<figure>
							<img src="<?php echo get_template_directory_uri(); ?>/img/imagem-sala.jpg">
							<figcaption class="hidden">imagem</figcaption>	
						</figure>
						<span>Saiba mais</span>
						<p>Desenvolvemos campanhas de mídia digital integrada usando uma combinação de Search, display, mídia social, email, e todas as outras ferramentas disponíveis.</p>
					</li>
					<li>
						<h4>Rh Lorem Ipsum</h4>
						<figure>
							<img src="<?php echo get_template_directory_uri(); ?>/img/imagem-sala.jpg">
							<figcaption class="hidden">imagem</figcaption>	
						</figure>
						<span>Saiba mais</span>
						<p>Desenvolvemos campanhas de mídia digital integrada usando uma combinação de Search, display, mídia social, email, e todas as outras ferramentas disponíveis.</p>
					</li>
					<li>
						<h4>Rh Lorem Ipsum</h4>
						<figure>
							<img src="<?php echo get_template_directory_uri(); ?>/img/imagem-sala.jpg">
							<figcaption class="hidden">imagem</figcaption>	
						</figure>
						<span>Saiba mais</span>
						<p>Desenvolvemos campanhas de mídia digital integrada usando uma combinação de Search, display, mídia social, email, e todas as outras ferramentas disponíveis.</p>
					</li>
					<li>
						<h4>Rh Lorem Ipsum</h4>
						<figure>
							<img src="<?php echo get_template_directory_uri(); ?>/img/imagem-sala.jpg">
							<figcaption class="hidden">imagem</figcaption>	
						</figure>
						<span>Saiba mais</span>
						<p>Desenvolvemos campanhas de mídia digital integrada usando uma combinação de Search, display, mídia social, email, e todas as outras ferramentas disponíveis.</p>
					</li>
					<li>
						<h4>Rh Lorem Ipsum</h4>
						<figure>
							<img src="<?php echo get_template_directory_uri(); ?>/img/imagem-sala.jpg">
							<figcaption class="hidden">imagem</figcaption>	
						</figure>
						<span>Saiba mais</span>
						<p>Desenvolvemos campanhas de mídia digital integrada usando uma combinação de Search, display, mídia social, email, e todas as outras ferramentas disponíveis.</p>
					</li>
					<li>
						<h4>Rh Lorem Ipsum</h4>
						<figure>
							<img src="<?php echo get_template_directory_uri(); ?>/img/imagem-sala.jpg">
							<figcaption class="hidden">imagem</figcaption>	
						</figure>
						<span>Saiba mais</span>
						<p>Desenvolvemos campanhas de mídia digital integrada usando uma combinação de Search, display, mídia social, email, e todas as outras ferramentas disponíveis.</p>
					</li>
					<li>
						<h4>Rh Lorem Ipsum</h4>
						<figure>
							<img src="<?php echo get_template_directory_uri(); ?>/img/imagem-sala.jpg">
							<figcaption class="hidden">imagem</figcaption>	
						</figure>
						<span>Saiba mais</span>
						<p>Desenvolvemos campanhas de mídia digital integrada usando uma combinação de Search, display, mídia social, email, e todas as outras ferramentas disponíveis.</p>
					</li>
					<li>
						<h4>Rh Lorem Ipsum</h4>
						<figure>
							<img src="<?php echo get_template_directory_uri(); ?>/img/imagem-sala.jpg">
							<figcaption class="hidden">imagem</figcaption>	
						</figure>
						<span>Saiba mais</span>
						<p>Desenvolvemos campanhas de mídia digital integrada usando uma combinação de Search, display, mídia social, email, e todas as outras ferramentas disponíveis.</p>
					</li>
					<li>
						<h4>Rh Lorem Ipsum</h4>
						<figure>
							<img src="<?php echo get_template_directory_uri(); ?>/img/imagem-sala.jpg">
							<figcaption class="hidden">imagem</figcaption>	
						</figure>
						<span>Saiba mais</span>
						<p>Desenvolvemos campanhas de mídia digital integrada usando uma combinação de Search, display, mídia social, email, e todas as outras ferramentas disponíveis.</p>
					</li>
				</ul>
			</div>
		</div>

	</section>

</div>

<?php
get_footer();
