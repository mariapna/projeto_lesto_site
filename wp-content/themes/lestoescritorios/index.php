<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package lestoescritorios
 */

get_header();
?>

<!-- PG DICAS -->
<div class="pg dicas">

	<!-- BANNER TOPO -->
	<section class="bannerPagina" style="background: url(<?php echo get_template_directory_uri(); ?>/img/bannerdicas.png);">
		<!-- <h2 class="nomePagina">Dicas</h2> -->
	</section>

	<!-- SESSÃO DICAS LESTO-->
	<section class="dicas-lesto">
		<h3 class="hidden">Dicas</h3>
		<div class="conteudo">
			<div class="titulo">
				<span>Fique por dentro de algumas dicas que podem favorecer o seu negocio</span>
			</div>

			<ul>
				<?php

				while (have_posts()):the_post();
					$imagemPost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0];

				?>
				<li>
					<a href="<?php echo get_permalink(); ?>">
						<h4><?php echo get_the_title(); ?></h4>
						<figure>
							<img src="<?php echo $imagemPost; ?>" alt="<?php echo get_the_title(); ?>">
							<figcaption class="hidden"><?php echo get_the_title(); ?></figcaption>	
						</figure>
						<span>Saiba Mais</span>
						<p><?php echo customExcerpt(300); ?></p>
					</a>
				</li>
				<?php endwhile; ?>
			</ul>
		</div>
	</section>

</div>

<?php
get_footer();
