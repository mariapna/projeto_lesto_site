<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package lestoescritorios
 */

global $configuracao;

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>

		<!-- META -->
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="">
		<meta name="keywords" content="">
		<meta property="og:title" content="" />
		<meta property="og:description" content="" />
		<meta property="og:url" content="" />
		<meta property="og:image" content=""/>
		<meta property="og:type" content="website" />
		<meta property="og:site_name" content="" />

		<!-- TÍTULO -->
		<title>Lesto Escritório</title>

		<!-- FONTS -->
		<link href="https://fonts.googleapis.com/css?family=Hammersmith+One|Rubik:300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">

		<script async defer crossorigin="anonymous" src="https://connect.facebook.net/pt_BR/sdk.js#xfbml=1&version=v4.0"></script>

		<!-- FAVICON -->
		<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico" /> 
		
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
		
	    <!-- TOPO -->
		<header class="topo">
			<div class="containerFull">
				<div class="row">
					
					<!-- LOGO -->
					<div class="col-sm-2">
						<a href="<?php echo home_url(); ?>">
							<img class="img-responsive" src="<?php echo $configuracao['opt_logo']['url'] ?>" alt="Logo Lesto Escritório">
						</a>
					</div>

					<!-- MENU  -->	
					<div class="col-sm-8">
						<div class="navbar" role="navigation">	
											
							<!-- MENU MOBILE TRIGGER -->
							<button type="button" id="botao-menu" class="navbar-toggle collapsed hvr-pop" data-toggle="collapse" data-target="#collapse">
								<span class="sr-only"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>

							<!--  MENU MOBILE-->
							<div class="row navbar-header">			
								<nav class="collapse navbar-collapse" id="collapse">
									<ul class="nav navbar-nav">			
										<!-- <li><a href="#" class="" >Home</a></li>			
										<li><a href="#" class="" >Planos</a></li>
										<li><a href="#" class="" >Salas</a></li>
										<li><a href="#" class="" >Unidades</a></li>
										<li><a href="#" class="" >Contato</a></li> -->
										<?php wp_nav_menu(); ?>
										<li>
											<a href="#" class="user" >
												<div class="lesto-icon-user">
									 				<img src="img/icon-user.png" alt="Icon User">
												</div>
											</a>
										</li>
									</ul>
								</nav>
							</div>			
						</div>
					</div>
					<!-- USER  -->	
					<div class="col-sm-2">
					</div>
				</div>
			</div>
		</header>