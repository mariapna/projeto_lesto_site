<?php

/**
* Template Name: Localização
* Description: Página Localização
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package lestoescritorios
*/

get_header();
?>

<!-- PG LOCALIZAÇÃO -->
<div class="pg localizacao">

	<!-- BANNER TOPO -->
	<section class="bannerPagina" style="background: url(<?php echo get_template_directory_uri(); ?>/img/bannerLocalizacao.jpg);">
		<!-- <h2 class="nomePagina">Localização</h2> -->
	</section>


	<section class="lesto-localizacao">
		<h3 class="hidden">localizacao</h3>

		<div class="horario-atendimento">
			<p>Venha nos fazer uma visita! Nosso horario de atendimento é de De seg. a Sex. das 08:00 ás 12:00 e das 13:30 ás 18:00</p>
		</div>

		<!-- BANNER UNIDADE -->
		<figure>
			<img src="<?php echo get_template_directory_uri(); ?>/img/bannerTopoHome.jpg">
			<figcaption class="hidden">Banner Topo </figcaption>	
		</figure>
		<div class="titulo-unidade">
			<h3>Unidade Centro</h3>
			<p>Avenida Candido de Abreu</p>
		</div>
		<div class="mapa">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d230483.77423019434!2d-49.4298859630679!3d-25.494740145206517!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dce35351cdb3dd%3A0x6d2f6ba5bacbe809!2sCuritiba%2C+PR!5e0!3m2!1spt-BR!2sbr!4v1564532449043!5m2!1spt-BR!2sbr" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>		    	

		<!-- BANNER UNIDADE -->
		<figure>
			<img src="<?php echo get_template_directory_uri(); ?>/img/banneLocal.jpg">
			<figcaption class="hidden">Banner Topo </figcaption>	
		</figure>
		<div class="titulo-unidade">
			<h3>Unidade Centro</h3>
			<p>Avenida Candido de Abreu</p>
		</div>
		<div class="mapa">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d230483.77423019434!2d-49.4298859630679!3d-25.494740145206517!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dce35351cdb3dd%3A0x6d2f6ba5bacbe809!2sCuritiba%2C+PR!5e0!3m2!1spt-BR!2sbr!4v1564532449043!5m2!1spt-BR!2sbr" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>

	</section>
</div>

<?php get_footer();