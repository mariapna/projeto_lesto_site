<?php



/**

 * Template Name: Inicial

 * Description: Página Inicial

 *

 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/

 *

 * @package lestoescritorios

 */



get_header();

?>



<!-- PG INICIAL -->

<div class="pg lesto-home">



	<!-- CARROSSEL DE DESTAQUE -->

	<section class="carrosselDestaque">

		<button class="carrosselDestaqueEsquerda"></button>

		<button class="carrosselDestaqueDireita"></button>

		<h6 class="hidden">Carrossel Destaque</h6>

		<div id="carrosselDestaque" class="owl-Carousel">

			

			<!-- ITEM -->

			<figure class="item">

				<a href="">

					<img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/banner.png" alt="Nome destaque">

				</a>

			</figure>



			<!-- ITEM -->

			<figure class="item">

				<a href="">

					<img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/img/banner.png" alt="Nome destaque">

				</a>

			</figure>



		</div>

	</section>



	<!-- SESSÃO QUEM SOMOS HOME-->

	<section class="lesto-quem-somos-home" >

		<div class="container">

			<h3 class="hidden"> Quem Somos Lesto</h3>

			<h3> Estabeleça seu escritório em um lugar privilegiado pagando muito pouco.</h3>

			<div class="col-md-8 ">

				<article class="lesto-texto">							

					<p>A lesto Escritório oferece opções de planos virtuais que atendem as necessidades de profissionais leberais ou autônomos até grande empresas.Nossas unidades oferecem estrutura corporativa de alto padrão, com salas de reunião, salas para atendimento e a disponibilização do endereço para divulgação comercial e fiscal e serviço de secretária para recepcionar seus clientes, atender suas ligações de forma personalizada, receber e entregar documentos.</p> <br>

					<span>Conheça abaixo nossos planos de escritório virtual e escolha o que melhor atende as suas necessidades</span>

				</article>

			</div>

			<div class="col-md-4">

				<div class="carrosselSalas sessao">

					<button class="carrosselSalasEsquerda"></button>

					<button class="carrosselSalasDireita"></button>

					<h6 class="hidden">Carrossel Salas</h6>

					<?php 	$fotosGaleriaIDs  = explode(',', $configuracao['opt_galeria_inicial']); ?>

					<div id="carrosselSalas" class="owl-Carousel">

						

						<?php 

						

							

							foreach($fotosGaleriaIDs as $fotosGaleria):



					        // $fotosGaleria = wp_get_attachment($fotosGaleria);

						 ?>

						<!-- ITEM -->

						<figure class="item">

							<a href="<?php echo wp_get_attachment_url( $fotosGaleria ); ?>">

								<img class="img-responsive" src="<?php echo wp_get_attachment_url( $fotosGaleria ); ?>" alt="<?php echo wp_get_attachment_url( $fotosGaleria ); ?>">

							</a>

						</figure>

						<?php endforeach; ?>



					</div>

				</div>

			</div>





		</div>				



	</section>



	<!-- SESSÃO PLANOS-->

	<section class="lesto-servicos-oferecidos">

		<div class="container">

			<h3 class="hidden">Serviços oferecidos</h3>



			<div class="titulo">

				<span> Planos </span>

			</div>



			<div class="lesto-servico">

				<ul>

					<?php 

						//LOOP DE POST DESTAQUE

						$planos = new WP_Query( array( 'post_type' => 'plano', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => 3) );

						while ( $planos->have_posts() ) : $planos->the_post();

							$plano_preco     = rwmb_meta('LestoEscritorios_plano_preco');

							$plano_descricao = rwmb_meta('LestoEscritorios_plano_descricao');

							$plano_icone = rwmb_meta('LestoEscritorios_plano_icone');

							

					?>

					<li>

						<div class="imgBackground" style="background: url(<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>);    ">
							<div class="opacity"></div>

							<figure>

								<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" style="display: none;">

								<figcaption class="hidden"><?php echo get_the_title() ?> </figcaption>	

							</figure>

						</div>

						<?php foreach ($plano_icone as $plano_icone):

							$plano_icone = $plano_icone['full_url'];

						 ?>

						<figure>

							<img src="<?php echo $plano_icone ?>">

							<figcaption class="hidden"><?php echo get_the_title() ?> </figcaption>	

						</figure>

						<?php endforeach; ?>

						<h2><?php echo get_the_title() ?></h2>

						<p><?php echo $plano_descricao ?></p>					

						<div class="btn-saiba-mais">

							

								<span>Saiba mais</span>

							

						</div>

						<div class="preco-mensal">

							<span> a partir de R$ <strong><?php echo $plano_preco  ?></strong></span>

						</div>

					</li>

					<?php  endwhile; wp_reset_query(); ?>

				</ul>					

			</div>



			<div class="titulo">

				<span> Salas </span>

			</div>

			<div class="lesto-servico">

				<ul>

					<?php 

						//LOOP DE POST DESTAQUE

						$salas = new WP_Query( array( 'post_type' => 'sala', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => 3) );

						while ( $salas->have_posts() ) : $salas->the_post();

							$salas_preco     = rwmb_meta('LestoEscritorios_preco_clientes_lesto');

							$sala_descricao = rwmb_meta('LestoEscritorios_sala_descricao');

							$salas_icone = rwmb_meta('LestoEscritorios_sala_icone');

							

					?>

					<li>

						<div class="imgBackground" style="background: url(<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>);   ">
                            <div class="opacity"></div>

							<figure>

								<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" style="display: none;">

								<figcaption class="hidden"><?php echo get_the_title() ?> </figcaption>	

							</figure>

						</div>

						<?php foreach ($salas_icone as $salas_icone):

							$salas_icone = $salas_icone['full_url'];

						 ?>

						<figure>

							<img src="<?php echo $salas_icone ?>">

							<figcaption class="hidden"><?php echo get_the_title() ?> </figcaption>	

						</figure>

						<?php endforeach; ?>

						<h2><?php echo get_the_title() ?></h2>

						<p><?php echo $sala_descricao ?></p>					

						<div class="btn-saiba-mais">

							

								<span>Saiba mais</span>

							

						</div>

						<div class="preco-mensal">

							<span> a partir de R$ <strong><?php echo $salas_preco  ?></strong></span>

						</div>

					</li>

					<?php  endwhile; wp_reset_query(); ?>					



				</ul>					

			</div>

		</section>



		<!-- SESSÃO UNIDADES  -->

		<section class="lesto-unidades">

			<h3 class="hidden"> Lesto Unidades </h3>

			<div class="container">

				<div class="titulo">

					<span> Unidades </span>

				</div>

				<?php 

					//LOOP DE POST DESTAQUE

					$i = 0;	

					$unidade = new WP_Query( array( 'post_type' => 'unidade', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );

					while ( $unidade->have_posts() ) : $unidade->the_post();

						if($i % 2 == 0):

				?>

				<div class="lesto-unidade">

					<div class="row">



						<!-- INFO  -->	

						<div class="col-md-4 " >

							<div class="lesto-info-home">

								<span> <?php echo get_the_title() ?> </span>

								<article class="lesto-formatacao-texto">

									<p><?php echo get_the_content() ?></p>

								</article>

								<div class="btn-saiba-mais">

									<span>Saiba mais</span>

								</div>

							</div>								

						</div>



						<!-- IMAGEM UNIDADE -->

						<div class="col-md-8">

							<figure>

								<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>">

								<figcaption class="hidden"><?php echo get_the_content() ?></figcaption>	

							</figure>

						</div>

					</div>

				</div>

				<?php else: ?>

				<div class="lesto-unidade">

					<div class="row">



						<!-- IMAGEM UNIDADE -->

						<div class="col-md-8 ">

							<figure>

								<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>">

								<figcaption class="hidden"><?php echo get_the_content() ?></figcaption>	

							</figure>

						</div>



						<!-- INFO  -->	

						<div class="col-md-4 ">

							<div class="lesto-info-home">

								<span> <?php echo get_the_title() ?> </span>

								<article class="lesto-formatacao-texto">

									<p><?php echo get_the_content() ?></p>

								</article>

								<div class="btn-saiba-mais">

									<span>Saiba mais</span>

								</div>

							</div>								

						</div>

					</div>

				</div>

				<?php endif; $i++;endwhile; wp_reset_query(); ?>

			</div>				

		</section>



		<!-- SESSÃO ESTATISTICAS  -->

        <!-- <section class="lesto-estatistica"> 

        	<h3 class="hidden"> Lesto estatistica </h3>

        	<div class="titulo">

				<span> Estatisticas </span>

			</div>	

			<div class="estatisticas"> 	

            	<div class="item">

            		<strong> 10%</strong>

            		<p>Lorem ipsum dolor set</p>

            	</div>

            	<div class="item">

            		<strong> 10%</strong>

            		<p>Lorem ipsum dolor set</p>

            	</div>

            	<div class="item">

            		<strong> 10%</strong>

            		<p>Lorem ipsum dolor set</p>

            	</div>

            </div>

        </section> -->



        <!-- SESSÃO FALE CONOSCO HOME -->

        <section class="lesto-fale-conosco">

        	<div class="container">

        		<form action="" method="post">

        			<div class="row">

        				<div class="form-titulo">

        					<span> Fale conosco </span>

        				</div>

        				<div class="form-subtitulo">

        					<span> Se interessou? Receba os valores no seu e-mail</span>

        					<p>Preencha o formulário para saber mais sobre os planos de coworking! Em breve, entramos em contato</p>

        				</div>

        				<div class="col-md-7">

        					<div class="form-info">

        						<!-- <div class="info">

        							<label for="nome"> Seu Nome</label>

        							<input type="text" id="nome" Placeholder="Nome" />

        						</div>

        						<div  class="info">

        							<label for="email">Seu e-email</label>

        							<input type="text" id="email" Placeholder="E-mail" />

        						</div>

        						<div  class="info full">

        							<label for="assunto">Assunto</label>

        							<input type="text" id="assunto" Placeholder="Assunto" />

        						</div>

        						<div class="info full textarea">

        							<label for="mensagem" >Mensagem</label>

        							<textarea Placeholder="Mensagem"> </textarea> 

        						</div>

        						<div class="btn-enviar">

        							<input type="submit"> </input>

        						</div> -->

        						<?php echo do_shortcode('[contact-form-7 id="5" title="Fomulário de contato"]'); ?>

        					</div>

        				</div>

        				<div class="col-md-5">

        					<div class="area-contato">

        						<div class="contato-titulo">

        							<span> Contato: </span>

        						</div>

        						<div class="lesto-outros-contatos">

        							<ul>

        								<li>

        									<div class="icone-contato">

        										<i class="fas fa-envelope"></i>

        									</div>

        									<span>contato@lesto.com.br</span>

        								</li>

        								<li>

        									<div class="icone-contato meio">

        										<i class="fas fa-phone"></i>		

        									</div>

        									<span>(41)3122-2000</span>

        								</li>

        								<li>

        									<div class="icone-contato">

        										<i class="fab fa-whatsapp"></i>

        									</div>

        									<span>(41)3122-2000</span>

        								</li>

        							</ul>							    	

        						</div>

        					</div>								

        				</div>	

        			</div>

        		</form>					

        	</div>	

        </div>			

    </section>

</div>



<?php get_footer();