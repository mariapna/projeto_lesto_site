<?php

/**
* Template Name: Contato
* Description: Página Contato
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package lestoescritorios
*/

get_header();
?>

<!-- PG CONTATO -->
<div class="pg contato">

	<!-- BANNER TOPO -->
	<section class="bannerPagina" style="background: url(<?php echo get_template_directory_uri(); ?>/img/bannerTopoHome.jpg);">
		<!-- <h2 class="nomePagina">Contato</h2> -->
	</section>

	<!-- SESSÃO FALE CONOSCO HOME -->
	<section class="lesto-fale-conosco">
		<div class="container">
			<form action="" method="post">
				<div class="row">
					<div class="form-titulo">
						<span> Fale conosco </span>
					</div>					
					<div class="col-md-6">
						<div class="form-info">
							<!-- <div class="info">
								<label for="nome"> Seu Nome</label>
								<input type="text" id="nome" Placeholder="Nome" />
							</div>
							<div  class="info">
								<label for="email">Seu e-email</label>
								<input type="text" id="email" Placeholder="E-mail" />
							</div>
							<div  class="info full">
								<label for="assunto">Assunto</label>
								<input type="text" id="assunto" Placeholder="Assunto" />
							</div>
							<div class="info full textarea">
								<label for="mensagem" >Mensagem</label>
								<textarea Placeholder="Mensagem"> </textarea> 
							</div>
							<div class="btn-enviar">
								<input type="submit"> </input>
							</div> -->
							<?php echo do_shortcode('[contact-form-7 id="5" title="Fomulário de contato"]'); ?>
						</div>
					</div>
					<div class="col-md-6">
						<div class="area-contato">
							<div class="contato-titulo">
								<span> Unidade Trajano: </span>
							</div>
							<div class="lesto-outros-contatos">
								<ul>
									<li>
										<div class="icone-contato">
											<i class="fas fa-envelope"></i>
										</div>
										<span>contato@lesto.com.br</span>
									</li>
									<li>
										<div class="icone-contato meio">
											<i class="fas fa-phone"></i>		
										</div>
										<span>(41)3122-2000</span>
									</li>
									<li>
										<div class="icone-contato">
											<i class="fab fa-whatsapp"></i>
										</div>
										<span>(41)3122-2000</span>
									</li>
								</ul>							    	
							</div>
						</div>
						<div class="area-contato">
							<div class="contato-titulo">
								<span> Unidade Neo </span>
							</div>
							<div class="lesto-outros-contatos">
								<ul>
									<li>
										<div class="icone-contato">
											<i class="fas fa-envelope"></i>
										</div>
										<span>contato@lesto.com.br</span>
									</li>
									<li>
										<div class="icone-contato meio">
											<i class="fas fa-phone"></i>		
										</div>
										<span>(41)3122-2000</span>
									</li>
									<li>
										<div class="icone-contato">
											<i class="fab fa-whatsapp"></i>
										</div>
										<span>(41)3122-2000</span>
									</li>
								</ul>							    	
							</div>
						</div>								
					</div>	
				</div>
			</form>					
		</div>	
	</section>
	<div class="mapa">
		<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d230483.77423019434!2d-49.4298859630679!3d-25.494740145206517!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94dce35351cdb3dd%3A0x6d2f6ba5bacbe809!2sCuritiba%2C+PR!5e0!3m2!1spt-BR!2sbr!4v1564532449043!5m2!1spt-BR!2sbr" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div>
</div>

<?php get_footer();