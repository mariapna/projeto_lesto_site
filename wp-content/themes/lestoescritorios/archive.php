<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package lestoescritorios
 */

get_header();
?>

<!-- PG DICAS -->
<div class="pg dicas">

	<!-- BANNER TOPO -->
	<section class="bannerPagina" style="background: url(img/bannerTopoHome.jpg);">
	</section>

	<!-- SESSÃO DICAS LESTO-->
	<section class="dicas-lesto">
		<h3 class="hidden">Dicas</h3>
		<div class="conteudo">
			<div class="titulo">
				<span>Fique por dentro de algumas dicas que podem favorecer o seu negocio</span>
			</div>

			<ul>
				<?php

				while (have_posts()):the_post();
					$imagemPost = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0];

					?>
					<li>
						<h4><?php echo get_the_title(); ?></h4>
						<figure>
							<img src="<?php echo $imagemPost; ?>" alt="<?php echo get_the_title(); ?>">
							<figcaption class="hidden"><?php echo get_the_title(); ?></figcaption>	
						</figure>
						<span>Saiba Mais</span>
						<p><?php echo customExcerpt(300); ?></p>
					</li>
				<?php endwhile; ?>
			</ul>
		</div>
	</section>
</div>

<?php
get_footer();