<?php

/**
 * Plugin Name: Base Lesto Escritórios
 * Description: Controle base do tema Lesto Escritórios.
 * Version: 0.1
 * Author: Agência HC Desenvolvimentos
 * Author URI: http://hcdesenvolvimentos.com.br
 * Licence: GPL2
 */

// Planos
// Salas
// Unidades

	function baseProjeto () {

		// TIPOS DE CONTEÚDO
		conteudosLestoEscritorios();

		taxonomiaLestoEscritorios();

		metaboxesLestoEscritorios();
	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/

	function conteudosLestoEscritorios(){

		// TIPOS DE CONTEÚDO
		tipoDestaque();

		tipoPlanos();

		tipoSala();

		tipoUnidades();

		/* ALTERAÇÃO DO TÍTULO PADRÃO */
		add_filter( 'enter_title_here', 'trocarTituloPadrao' );
		function trocarTituloPadrao($titulo){

			switch (get_current_screen()->post_type) {

				case 'destaque':
					$titulo = 'Título do destaque';
				break;
				
				default:
				break;
			}

		    return $titulo;

		}

	}

	// CUSTOM POST TYPE DESTAQUES
	function tipoDestaque() {

		$rotulosDestaque = array(
								'name'               => 'Destaques',
								'singular_name'      => 'destaque',
								'menu_name'          => 'Destaques',
								'name_admin_bar'     => 'Destaques',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo destaque',
								'new_item'           => 'Novo destaque',
								'edit_item'          => 'Editar destaque',
								'view_item'          => 'Ver destaque',
								'all_items'          => 'Todos os destaques',
								'search_items'       => 'Buscar destaque',
								'parent_item_colon'  => 'Dos destaques',
								'not_found'          => 'Nenhum destaque cadastrado.',
								'not_found_in_trash' => 'Nenhum destaque na lixeira.'
							);

		$argsDestaque 	= array(
								'labels'             => $rotulosDestaque,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-megaphone',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'destaque' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','editor','thumbnail')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('destaque', $argsDestaque);

	}

	// CUSTOM POST TYPE PLANOS
	function tipoPlanos() {

		$rotulosPlano = array(
								'name'               => 'Planos',
								'singular_name'      => 'plano',
								'menu_name'          => 'Planos',
								'name_admin_bar'     => 'Planos',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo plano',
								'new_item'           => 'Novo plano',
								'edit_item'          => 'Editar plano',
								'view_item'          => 'Ver plano',
								'all_items'          => 'Todos os planos',
								'search_items'       => 'Buscar plano',
								'parent_item_colon'  => 'Dos planos',
								'not_found'          => 'Nenhum plano cadastrado.',
								'not_found_in_trash' => 'Nenhum plano na lixeira.'
							);

		$argsPlano 	= array(
								'labels'             => $rotulosPlano,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 5,
								'menu_icon'          => 'dashicons-feedback',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'plano' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','editor','thumbnail')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('plano', $argsPlano);

	}

	// CUSTOM POST TYPE SALAS
	function tipoSala() {

		$rotulosSala = array(
								'name'               => 'Salas',
								'singular_name'      => 'sala',
								'menu_name'          => 'Salas',
								'name_admin_bar'     => 'Salas',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo sala',
								'new_item'           => 'Novo sala',
								'edit_item'          => 'Editar sala',
								'view_item'          => 'Ver sala',
								'all_items'          => 'Todos os salas',
								'search_items'       => 'Buscar sala',
								'parent_item_colon'  => 'Dos salas',
								'not_found'          => 'Nenhum sala cadastrado.',
								'not_found_in_trash' => 'Nenhum sala na lixeira.'
							);

		$argsSala 	= array(
								'labels'             => $rotulosSala,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 6,
								'menu_icon'          => 'dashicons-admin-multisite',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'sala' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','editor','thumbnail')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('sala', $argsSala);

	}

	// CUSTOM POST TYPE UNIDADES
	function tipoUnidades() {

		$rotulosUnidade = array(
								'name'               => 'Unidades',
								'singular_name'      => 'unidade',
								'menu_name'          => 'Unidades',
								'name_admin_bar'     => 'Unidades',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo unidade',
								'new_item'           => 'Novo unidade',
								'edit_item'          => 'Editar unidade',
								'view_item'          => 'Ver unidade',
								'all_items'          => 'Todos os unidades',
								'search_items'       => 'Buscar unidade',
								'parent_item_colon'  => 'Dos unidades',
								'not_found'          => 'Nenhum unidade cadastrado.',
								'not_found_in_trash' => 'Nenhum unidade na lixeira.'
							);

		$argsUnidade 	= array(
								'labels'             => $rotulosUnidade,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 7,
								'menu_icon'          => 'dashicons-building',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'unidade' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','editor','thumbnail')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('unidade', $argsUnidade);

	}


	/****************************************************
	* TAXONOMIA
	*****************************************************/
	function taxonomiaLestoEscritorios() {		
		taxonomiaCategoriaSalas();
	}

		// TAXONOMIA DE SALAS
		function taxonomiaCategoriaSalas() {

			$rotulosCategoriaSalas = array(
				'name'              => 'Categorias de salas',
				'singular_name'     => 'Categoria de salas',
				'search_items'      => 'Buscar categorias de salas',
				'all_items'         => 'Todas as categorias de salas',
				'parent_item'       => 'Categoria de salas pai',
				'parent_item_colon' => 'Categoria de salas pai:',
				'edit_item'         => 'Editar categoria de salas',
				'update_item'       => 'Atualizar categoria de salas',
				'add_new_item'      => 'Nova categoria de salas',
				'new_item_name'     => 'Nova categoria',
				'menu_name'         => 'Categorias de salas',
				);

			$argsCategoriaSalas 	= array(
				'hierarchical'      => true,
				'labels'            => $rotulosCategoriaSalas,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'categoria-salas' ),
				);

			register_taxonomy( 'categoriaSalas', array( 'sala' ), $argsCategoriaSalas );

		}			

    /****************************************************
	* META BOXES
	*****************************************************/
	function metaboxesLestoEscritorios(){

		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );

	}

		function registraMetaboxes( $metaboxes ){

			$prefix = 'LestoEscritorios_';

			// METABOX DE DESTAQUE
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxDestaqueInicial',
				'title'			=> 'Detalhes do Destaque',
				'pages' 		=> array( 'destaque' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Link destaque: ',
						'id'    => "{$prefix}link_destaque",
						'desc'  => 'Link destaque',
						'type'  => 'text'
					),	
				),
			);

			// METABOX DE UNIDADES
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxUnidades',
				'title'			=> 'Detalhes da Unidade',
				'pages' 		=> array( 'unidade' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Endereço unidade: ',
						'id'    => "{$prefix}endereco_unidade",
						'desc'  => 'Rua x, centro, 123',
						'type'  => 'text'
					),		
									
				),
			);

			// METABOX DE SALAS
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxSalas',
				'title'			=> 'Detalhes da Sala',
				'pages' 		=> array( 'sala' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Preço da sala para clientes lesto: ',
						'id'    => "{$prefix}preco_clientes_lesto",
						'desc'  => 'R$14,00/hora',
						'type'  => 'text'
					),

					array(
						'name'  => 'Preço da sala para clientes externos: ',
						'id'    => "{$prefix}preco_clientes_externos",
						'desc'  => 'R$20,00/hora',
						'type'  => 'text'
					),		
									
				),
			);

			return $metaboxes;
		}

		function metaboxjs(){

			global $post;
			$template = get_post_meta($post->ID, '_wp_page_template', true);
			$template = explode('/', $template);
			$template = explode('.', $template[1]);
			$template = $template[0];

			if($template != ''){
				wp_enqueue_script( 'metabox-js', get_template_directory_uri() 	. '/js/metabox_' . $template . '.js', array('jquery'), '1.0', true);
			}
		}

	/****************************************************
	* SHORTCODES
	*****************************************************/
	function shortcodesProjeto(){

	}

	/****************************************************
	* ATALHOS VISUAL COMPOSER
	*****************************************************/
	function visualcomposerProjeto(){

	    if (class_exists('WPBakeryVisualComposer')){

		}

	}

  	/****************************************************
	* AÇÕES
	*****************************************************/

	// INICIA A FUNÇÃO PRINCIPAL
	add_action('init', 'baseProjeto');

	// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
	add_action( 'add_meta_boxes', 'metaboxjs');

	// FLUSHS
	function rewrite_flush() {

    	baseProjeto();

   		flush_rewrite_rules();
	}

	register_activation_hook( __FILE__, 'rewrite_flush' );